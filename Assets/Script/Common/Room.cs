﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ROOM_STATUS{
	UN_READY= 0, 		//"等待用户准备",
	READY= 1,		 	//"所有用户已准备"
	GAMEING= 2, 		//"游戏中"
	COM_SCORES= 3, 		//"游戏尾期，统计分数与结果"
	SHOW_SCORES= 4, 	//"游戏尾期，显示结果"
}

public enum PLAYER_STATUS{
	FREE = 0,			// "空闲",
	UN_READY = 1,		// "未准备",
	READY=2 ,			//"已准备",
	WAIT_ACTION=3,		// "等待动作",
	CAN_ACTION=4 ,		//"可动作",
	BEFORE_SUBMIT=5 ,	//"正在摆牌",
	AFTER_SUBMIT=6 ,	//"等待其它玩家提交摆牌结果",
	IN_QUEUE=7 ,		//"匹配队列中",
	BEFORE_QUEUE_TO_ROOM=8,// "等待确认匹配",
	AFTER__QUEUE_TO_ROOM=9//"确认匹配，等待其它玩家确认",
}


public class Room{

	public string id;
	public string hashId;
	public int maxRound;
	public int curRound;
	public int status;
	public bool withLaizi;
	public string extendColor1;
	public string extendColor2;
	public List<User> players = new List<User> ();
	public User owner;
	public JSONObject data;
	public string payMethod;
	public int playerNum;
	public bool canDissoluton;
	public int withJiasanzhang;
	public int payPrice;

	public Room(JSONObject json){
		this.id = json["id"].str;
		this.hashId = json["hash_id"].str;
		this.maxRound = int.Parse(json["max_round"].ToString());
		this.curRound = int.Parse(json["cur_round"].ToString());
		this.status = int.Parse(json["status"].ToString());
		this.withLaizi = bool.Parse(json["with_laizi"].ToString());
		this.extendColor1 = json ["extend_color_1"].str;
		this.extendColor2 = json["extend_color_2"].str;
		this.canDissoluton = bool.Parse (json ["can_disband_room"].ToString ());
		this.withJiasanzhang = int.Parse (json ["with_jiasanzhang"].ToString ());
		this.payPrice = int.Parse (json ["pay_price"].ToString ());

		List<JSONObject> _users = json ["players"].list;
		foreach (var user in _users) {
			User u = new User(user);
			this.players.Add (u);
		}
		lock(players){
			players.Sort (new UserComparer());	
		}

		if(!json["owner"].IsNull)
			this.owner = new User (json["owner"]);

		this.data = json ["data"];
		this.payMethod = json ["pay_method"].str;
		this.playerNum = int.Parse (json ["player_number"].ToString ());
	}
}


