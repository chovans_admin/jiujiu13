﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class SDKTool : MonoBehaviour {

	//导出按钮以后将在xcode项目中生成这个按钮的注册，
	//这样就可以在xocde代码中实现这个按钮点击后的事件。
	[DllImport("__Internal")]
	private static extern void _PressButton ();
	[DllImport("__Internal")]
	private static extern void _ShareFriend();
	[DllImport("__Internal")]
	private static extern void _ShareZone();
	[DllImport("__Internal")]
	private static extern void _ShareResult(string param);
	[DllImport("__Internal")]
	private static extern void _AliPay (string param);
	[DllImport("__Internal")]
	private static extern void _WxPay (string param);
	[DllImport("__Internal")]
	private static extern void _Invite (string param);

	public static void Login ()
	{

		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_PressButton ();
		}
	}

	public static void ShareFriend(){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_ShareFriend ();
		}
	}

	public static void ShareZone(){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_ShareZone ();
		}
	}

	public static void AliPay(string param){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_AliPay (param);
		}
	}

	public static void WxPay(string param){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_WxPay (param);
		}
	}

	public static void ShareResult(string param){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_ShareResult (param);
		}
	}
	public static void Invite(string param){
		if (Application.platform != RuntimePlatform.OSXEditor) 
		{
			_Invite (param);
		}
	}
}
