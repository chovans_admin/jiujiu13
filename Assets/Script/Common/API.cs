﻿using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class API : MonoBehaviour {

	//错误事件
	public static string ERROR_ACTION = "ErrorAction";
	//对战记录
	public static string GAME_LOGS = "GameLogs";
	//公告
	public static string NOTICE_ACTION = "NoticeAction";
	//文字消息
	public static string TEXT_MESSAGE = "TextMessage";
	//房间配置信息
	public static string ROOM_SETTING = "RoomSetting";
	//通知其他玩家解散房间
	public static string DISSOLUTION_ROOM_START = "DissolutionRoom";
	//解散房间失败
	public static string DISSOLUTION_FAIL = "DissolutionFail";
	//语音消息
	public static string VOICE_MESSAGE = "VoiceMessage";
	//排队列表
	public static string QUEUE_SIZE = "QueueSize";
	//获取商品
	public static string GOODS = "Goods";
	//购买商品成功
	public static string BUYGOODSSUCCESS = "BuyGoodsSuccess";
	//购买商品失败
	public static string BUYGOODSERROR = "BuyGoodsError";
	//牌局统计
	public static string ROOMSTATISTICSRES = "RoomStatisticsRes";
	//获取积分商品
	public static string POINTGOODS = "POINTGOODS";
	//微信登录
	public static string WX_LOGIN = "weixinLogin";
	//重新设置session
	public static string RESET_SESSION = "RESETSESSION";
	//续费成功
	public static string RECHARGE_SUCCESS = "RechargeSuccess";
	//登录
	public static string USER_ACTION = "UserFrame";
	//获取房费
	public static string PAY_PRICE = "PayPrice";
	//检测是否特殊牌
	public static string CHECK_SPECIAL = "CheckSpecial";
	//获取总分
	public static string STASTICS_ROOM = "StasticsRoom";

	public static void Login(string code){
		Debug.Log ("登录");

		Dictionary<string, string> data = new Dictionary<string, string>();
		data["wx_code"] = code;
		#if UNITY_ANDROID || UNITY_IOS
		if(!code.Contains("."))
			data ["env"] = "NATIVE";
		#endif
//		Main.Instance.SocketEmit("user-login",new JSONObject(data));
		if(!Main.loginSuccess)
			Main.Instance.SocketEmit("user-login",new JSONObject(data));
	}

	//创建房间
	public static string ROOM_ACTION = "RoomFrame";
	public static string ROOM_EXIT_ACTION = "RoomExit";
	public class CREATE_ROOM_PARAM{
		public static string Payment_Master = "房主支付";
		public static string Payment_Flat = "房费平摊";
	}
	public static void CreateRoom(int playerNum,bool isLaizi,int maxRound,string extendColor1,
		string extendColor2,string payment,string mode,bool isShowRestCard,int type,int pokers,bool isCreate){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["mode"] = mode;
		data ["player_number"] = playerNum.ToString();
		data ["pay_method"] = payment;
		data ["with_laizi"] = isLaizi.ToString();
		data ["max_round"] = maxRound.ToString();

		//加一色,5人场,6人场
		if (type == 1 || type == 3 || type == 4) {
			data ["extend_color_1"] = extendColor1;
		}
		//癞子
		else if (type == 2) {
			data ["is_show_rest_cards"] = isShowRestCard.ToString();
		}
		//6人场
		if(type == 4){
			data ["extend_color_2"] = extendColor2;
		}
		//加3张加4张
		if (type == 5) {
			data ["with_jiasanzhang"] = pokers.ToString();
		}
		if (isCreate) {
			Main.Instance.SocketEmit ("create-room", new JSONObject (data));
		} else {
			Main.Instance.SocketEmit ("payprice", new JSONObject (data));
		}

	}

	//检测是否在房间内
	public static void CheckIsInRoom(){
		Main.Instance.SocketEmit ("room-frame", new JSONObject ());
	}

	//加入匹配队列
	public static void JoinQueue(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["player_number"] = "4";
		Main.Instance.SocketEmit ("join-queue", new JSONObject (data));
	}
	//确认队列
	public static void QueueComfirm(){
		Main.Instance.SocketEmit ("confirm-queue-to-room", new JSONObject ());
	}
	//取消队列
	public static void CancelConfirm(){
		Main.Instance.SocketEmit ("cancel-queue", new JSONObject ());
	}

	//加入房间
	public static void JoinRoom(string roomNumber){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["room_id"] = roomNumber;
		Main.Instance.SocketEmit ("join-room", new JSONObject (data));
	}

	//改变状态
	public static void ChangeRoomStatus(bool isReady){
		if(isReady)
			Main.Instance.SocketEmit ("ready", new JSONObject ());
		else
			Main.Instance.SocketEmit ("not-ready", new JSONObject ());
	}

	//提交牌
	public static void Submit(string[] top,string[] mid,string[] btm){
		SubmitEntity entity = new SubmitEntity ();
		entity.top = top;
		entity.mid = mid;
		entity.btm = btm;
		Main.Instance.SocketEmit ("submit-res", JSONObject.Create (JsonUtility.ToJson(entity)));
	}

	//重新开始
	public static void RestartGame(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["auto_ready"] = "true";
		Main.Instance.SocketEmit ("restart", new JSONObject (data));
	}

	//续费
	public static void RechargeGame(){
		Main.Instance.SocketEmit ("recharge-room",new JSONObject());
	}

	//离开房间
	public static void LeaveRoom(){
		Debug.Log ("离开房间");
		Main.Instance.SocketEmit ("leave-room",new JSONObject());
	}

	//改变牌型  测试使用
	public static void ChangeCard(){
		string cardlist = "[\"红心_A\",\"红心_A\",\"红心_A\",\"红心_A\",\"红心_A\",\"红心_6\",\"红心_7\",\"红心_8\",\"红心_9\",\"红心_10\",\"红心_J\",\"红心_Q\",\"红心_K\"]";
		Main.Instance.SocketEmit ("--changed-card-list--",new JSONObject(cardlist));
	}

	//获取战绩
	public static void GetGameLog(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["from"] = "0";
		data ["limit"] = "20";
		Main.Instance.SocketEmit ("get-user-game-logs",new JSONObject(data));
	}

	//获取公告
	public static void GetNotice(){
		Debug.Log ("请求公告");
		Main.Instance.SocketEmit ("bulletin-json", new JSONObject ());
	}

	//发送消息
	public static void SendTextMessage(string msg){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["msg"] = msg;
		Main.Instance.SocketEmit ("send-msg",new JSONObject(data));
	}

	//获取房间配置
	public static void GetRoomSetting(){
		Main.Instance.SocketEmit ("room-setting",new JSONObject());
	}

	//发起解散
	public static void Dissolution(){
		Main.Instance.SocketEmit ("disbanded-room", new JSONObject ());
	}

	//拒绝解散
	public static void DisagreeDissolution(){
		Main.Instance.SocketEmit ("disbanded-room-disagree", new JSONObject ());
	}

	//发送语音消息
	public static void SendAudio(string bytes,float length){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["length"] = length.ToString();
		data ["voice"] = bytes;
		Main.Instance.SocketEmit ("send-voice", new JSONObject (data));
	}

	//提交特殊牌型
	public static void SubmitSpecial(){
		SubmitEntity entity = new SubmitEntity ();
		if (LicensingCompent.SpecialResInfo == null) {
			return;
		}
//		Is_SpecialResInfo specialInfo = Utils.getSpecial();

		if (LicensingCompent.SpecialResInfo["combinations"].list.Count == 0) {
			entity.all = Main.user.cardList.ToArray ();
		} else {
			entity.top = Utils.CartToText (LicensingCompent.SpecialResInfo["combinations"] [0]["Top"].list);
			entity.mid = Utils.CartToText (LicensingCompent.SpecialResInfo["combinations"] [0]["Mid"].list);
			entity.btm = Utils.CartToText (LicensingCompent.SpecialResInfo["combinations"] [0]["Btm"].list);
			entity.all = new string[0];
		}
		entity.type = LicensingCompent.SpecialResInfo["type"].str;
//		Debug.Log (JSONObject.Create (JsonUtility.ToJson(entity)).ToString());

		Main.Instance.SocketEmit ("submit-res", JSONObject.Create (JsonUtility.ToJson(entity)));
	}

	//获取商品列表
	public static void GetGoods(){
		Main.Instance.SocketEmit ("goods",new  JSONObject());
	}


	//获取积分换砖石列表
	public static void PointGetGoods(){
		Main.Instance.SocketEmit ("pointgoods",new  JSONObject());
	}

	//发送支付宝请求
	public static void BuyGoods(string goodsId,bool isAlipay){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["pay_method"] = isAlipay ? "手机支付宝" : "微信";
		data ["goods_id"] = goodsId;
		Main.Instance.SocketEmit ("buy-goods", new JSONObject (data));
	}

	public static void BuyPointGoods(string pointGoodsId){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["pointgoods_id"] = pointGoodsId;
		Main.Instance.SocketEmit ("buy-pointgoods", new JSONObject (data));
	}

	//绑定邀请码
	public static void BindCode(string code){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data ["share_code"] = code;
		Main.Instance.SocketEmit ("bind-share-code", new JSONObject (data));
	}
	//获取统计总分
	public static void GetStatistics(){
		Main.Instance.SocketEmit ("statistics-room", new JSONObject ());
	}

	// 检测是否特殊牌
	public static void CheckSpecial(){
		Dictionary<string, string[]> data = new Dictionary<string, string[]>();
		string cardlist = String.Join ("\\\",\\\"", Main.user.cardList.ToArray ());
		cardlist = "[\\\"" + cardlist + "\\\"]";
		data ["card_list"] = Main.user.cardList.ToArray();

		List<JSONObject> pokers = new List<JSONObject>();
		for (int i = 0; i < Main.user.cardList.Count; i++) {
			string p = Main.user.cardList [i];
			JSONObject poker = JSONObject.CreateStringObject (p);
			pokers.Add(poker);
		}

		JSONObject j = new JSONObject(JSONObject.Type.OBJECT);
		JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);
		arr.list = pokers;
		j.AddField("card_list", arr);

		Main.Instance.SocketEmit ("is-special-card-list", j);
	}
}
