﻿using System;
using System.Collections.Generic;

    public class Card
    {

        public enum CardColor { Red, Black };
        public static CardColor GetColorByType(string type)
        {
            if (type.StartsWith("大") || type.StartsWith("红心") || type.StartsWith("方块"))
            {
                return CardColor.Red;
            }
            return CardColor.Black;
        }
        static Func<string, int> getHashValGenerator()
        {
            Dictionary<string, int> hashValMap = new Dictionary<string, int>();
            return (hash) =>
            {
                int res;
                if (!hashValMap.TryGetValue(hash, out res))
                {
                    res = hashValMap.Count + 1;
                    hashValMap.Add(hash, res);
                }
                return res;
            };
        }
        static Dictionary<string, int> numberHashValMap = new Dictionary<string, int>();
        static int getNumberVal(string hash)
        {
            int res;
            if (!numberHashValMap.TryGetValue(hash, out res))
            {
                res = numberHashValMap.Count + 1;
                numberHashValMap.Add(hash, res);
            }
            return res;
        }
        static Dictionary<string, int> typeHashValMap = new Dictionary<string, int>();
        static int getTypeVal(string hash)
        {
            if (hash.Length>2)
            {
                hash = hash.Substring(0, 2);
            }
            int res;
            if (!typeHashValMap.TryGetValue(hash, out res))
            {
                res = typeHashValMap.Count + 1;
                typeHashValMap.Add(hash, res);
            }
            return res;
        }

        public Card(string type, string number, bool is_any = false)
        {
            Type = type;
            Number = number;
            type_val = getTypeVal(type);
            val = getNumberVal(number);
            if (val == 1)//A
            {
                Weight = 14;
            }
            else if (val == 14) //王
            {
                Weight = 15;
            }
            else
            {
                Weight = val;
            }
            Color = GetColorByType(type);
            txt = type + "_" + number;
            IsAny = is_any;
        }
        public readonly CardColor Color;
        public readonly string Type;
        public readonly string Number;
        public readonly int Weight;
        public bool IsAny;// 癞子
        private readonly int type_val;
        private readonly int val;
        private readonly string txt;
        public int ValueOfType()
        {// 这个和Type不一样，比如“红心”与“红心*”两个Type不一样，但是ValueOfType是一样的。
            return this.type_val;
        }
        public int ValueOf()
        {
            return this.val;
        }
        public override string ToString()
        {
            return this.txt;
        }
        /// <summary>
        /// 癞子转化成对应的权重
        /// </summary>
        /// <returns></returns>
        public int IgnoreAnyWeight(int if_any_default_weight = 14) {
            if (IsAny)
            {
                return if_any_default_weight;//A
            }
            return Weight;
        }

		static Dictionary<string, Card> CARD_INSTANCE_CACHE = new Dictionary<string, Card>();
        static public Card BuildIns(string type, string number, bool force_build = false)
        {
            var txt = type + "_" + number;
            Card res;
            if (force_build)
            {
                res = new Card(type, number);
            }
            else
            {
                if (!CARD_INSTANCE_CACHE.TryGetValue(txt, out res))
                {
                    res = new Card(type, number);
                    CARD_INSTANCE_CACHE.Add(txt, res);
                }
            }

            return res;
        }
        static public Card GetIns(string txt)
        {
            Card res;
            CARD_INSTANCE_CACHE.TryGetValue(txt, out res);
            return res;
        }
    }
	
public enum CartType{
	top_three = 55,//冲三
	mid_full_house = 88,//中盾葫芦
	five = 11,//五同
	straight_flush= 10, //同花顺
	four= 9, //铁支
	full_house= 8, //葫芦 。中盾葫芦
	flush= 7, //同花
	straight= 6, //顺子
	three= 5, //三条。冲三
	two_pair= 4, //两对
	two= 3, //对子
	any= 2, //散牌
	three_straight= 21, // 三顺子
	three_flush= 22, // 三同花
	six_pair= 23, // 六对半
	all_small= 24, // 全小
	all_big= 25, // 全大
	one_long= 26, // 一条龙
	king_long= 27, // 至尊青龙
}