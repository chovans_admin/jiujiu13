﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    public class CardBox
    {
        static readonly CardList NORMAL_CARD_LIST = Init_NORMAL_CARD_LIST();
        static readonly CardList LAIZI_CARD_LIST = Init_LAIZI_CARD_LIST();
        static readonly Dictionary<string, CardList> EXTEND_CARD_LIST = Init_EXTEND_CARD_LIST();
        static CardList Init_NORMAL_CARD_LIST(bool is_force_build = false)
        {
            var res = new CardList();

            var types = new string[] { "黑桃", "红心", "梅花", "方块" };
            var numbers = new string[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
            foreach (var type in types)
            {
                foreach (var number in numbers)
                {
                    res.Add(Card.BuildIns(type, number, is_force_build));
                }
            }
            return res;
        }
        static CardList Init_LAIZI_CARD_LIST()
        {
            var res = new CardList();
            var small_king_card = Card.BuildIns("小", "王");
            small_king_card.IsAny = true;
            res.Add(small_king_card);
            var big_king_card = Card.BuildIns("大", "王");
            big_king_card.IsAny = true;
            res.Add(big_king_card);
            return res;
        }
        static Dictionary<string, CardList> Init_EXTEND_CARD_LIST()
        {
            var res = new Dictionary<string, CardList>();
            var types = new string[] { "黑桃*", "红心*", "梅花*", "方块*" };
            var numbers = new string[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
            foreach (var type in types)
            {
                var card_list = new CardList();
                res.Add(type, card_list);
                foreach (var number in numbers)
                {
                    card_list.Add(Card.BuildIns(type, number));
                }
            }
            return res;
        }

        public CardBox(bool use_laizi = false, string extend_color_1 = "", string extend_color_2 = "")
        {
            this.use_laizi = use_laizi;
            this.extend_color_1 = extend_color_1;
            this.extend_color_2 = extend_color_2;
        }
        public bool use_laizi = false;
        public string extend_color_1 = "";
        public string extend_color_2 = "";
        public CardList card_list = new CardList();
        public void Init()
        {
            this.InitCards();
            this.RandomCards();
        }
        public void InitCards()
        {
            this.card_list.Clear();
            this.card_list.AddRange(NORMAL_CARD_LIST);
            if (this.use_laizi)
            {
                this.card_list.AddRange(LAIZI_CARD_LIST);
            }
            if (this.extend_color_1 != "" && EXTEND_CARD_LIST.ContainsKey(this.extend_color_1))
            {
                this.card_list.AddRange(EXTEND_CARD_LIST[this.extend_color_1]);
            }
            if (this.extend_color_2 != "" && EXTEND_CARD_LIST.ContainsKey(this.extend_color_2))
            {
                this.card_list.AddRange(EXTEND_CARD_LIST[this.extend_color_2]);
            }
        }
        public void RandomCards()
        {
            var random = new Random();
            for (int i = 0, len = this.card_list.Count; i < len; i++)
            {
                var random_index = random.Next(len);
                var card = this.card_list[i];
                this.card_list.RemoveAt(i);
                this.card_list.Insert(random_index, card);
            }

        }
    }

