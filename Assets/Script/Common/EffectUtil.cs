﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectUtil : MonoBehaviour {

	private static string getPath(int sex){
		return sex == 0?"Woman/":"Man/";
	}

	//开始比牌
	public static void KaishiBiPai(int sex){
		EffectManager.Instance.Play (getPath (sex) + "kaishibipai");
	}

	//发牌
	public static void FaPai(){
		EffectManager.Instance.Play ("fapai");
	}

	//请出牌
	public static void QingChuPai(){
		EffectManager.Instance.Play (getPath (Main.user.sex) + "qingchupai");
	}

	//点牌
	public static void DianPai(){
		EffectManager.Instance.Play ("dianpai");
	}

	//打枪
	public static void DaQiang(int sex){
		EffectManager.Instance.Play (getPath (sex) + "daqiang");
	}
	//打枪音效
	public static void DaQiangYinXiao(){
		EffectManager.Instance.Play ("daqiang");
	}
	//全垒打
	public static void QuanLeiDa(int sex){
		EffectManager.Instance.Play (getPath (sex) + "quanleida");
	}

	public void DaQiang2(){
		//延时动画
		StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
			{
				EffectManager.Instance.Play ("fapai");
			},1f));
	}

	public static void PlayWithPaixing(string paixing,int sex,AudioSource audioSource){
		int type = int.Parse (paixing);
		string audioName = null;
		//对子
		if (type == CartType.two.GetHashCode ()) {
			audioName = "duizi";
		} else if (type == CartType.two_pair.GetHashCode ()) {
			audioName = "liangdui";
		} else if (type == CartType.two_pair.GetHashCode ()) {
			audioName = "liangdui";
		} else if (type == CartType.three.GetHashCode ()) {
			audioName = "santiao";
		} else if (type == CartType.straight.GetHashCode ()) {
			audioName = "shunzi";
		} else if (type == CartType.flush.GetHashCode ()) {
			audioName = "tonghua";
		} else if (type == CartType.full_house.GetHashCode ()) {
			audioName = "hulu";
		} else if (type == CartType.four.GetHashCode ()) {
			audioName = "tiezhi";
		} else if (type == CartType.straight_flush.GetHashCode ()) {
			audioName = "tonghuashun";
		} else if (type == CartType.any.GetHashCode ()) {
			audioName = "wulong";
		} else if (type == CartType.five.GetHashCode ()) {
			audioName = "wutong";
		} else if (type == CartType.mid_full_house.GetHashCode ()) {
			audioName = "zhongdunhulu";
		} else if (type == CartType.top_three.GetHashCode ()) {
			audioName = "chongsan";
		}

		if (audioName == null) {
			Debug.Log ("没有匹配到牌型声音：" + paixing);
		} else if(PlayerPrefs.GetInt (Main.EFFECT) != 0){
			AudioClip ac = Resources.Load("Music/"+getPath(sex)+audioName) as AudioClip;
			audioSource.clip = ac;
			audioSource.mute = false;
			audioSource.volume = 1f;
			audioSource.Play ();
		}
	}

	public static void PlayWithSpecial(string paixing,int sex){
		EffectManager.Instance.Play (getPath(sex)+paixing);
	}

	public static void TeShuPaiXing(string name){
		EffectManager.Instance.Play (getPath(Main.user.sex)+name);
	}

	public static void DianJi(){
		if (PlayerPrefs.GetInt (Main.EFFECT) == 1) {
			EffectManager.Instance.Play ("dianji");	
		}
	}

	public static Dictionary<string,string> msgVoice = new Dictionary<string,string>(){
		{"哎呀，七十岁大爷出牌都比你快","1"},
		{"唉，你慢慢配牌，我去海边吹下风","2"},
		{"急什么，让我想想怎么打","3"},
		{"急急急，赶着去投胎","4"},
		{"这么久没见了，最近在忙什么","5"},
		{"来来来，决战到天亮","6"},
		{"你牌玩得这么好，你爸妈知道嘛","7"},
		{"我是神枪手，打枪本领强","8"},
		{"今天忘记洗手了","9"},
		{"颤抖把，这把要全垒打","10"},
		{"不好意思，我接个电话","11"},
		{"我有事先走了，下次再玩吧","12"}
	};

	//播放聊天消息
	public static void playMsgVoice(string msg,AudioSource audioSource){

		if (PlayerPrefs.GetInt (Main.EFFECT) == 0) {
			return;
		}

		string idx = "0";
		msgVoice.TryGetValue (msg, out idx);
		if (idx == "0") {
			Debug.Log ("没有找到匹配的语音消息");
		} else {
			AudioClip ac = Resources.Load("Music/Msg Voice/"+idx) as AudioClip;
			if (ac != null) {
				audioSource.clip = ac;
				audioSource.mute = false;
				audioSource.volume = 1f;
				audioSource.Play ();
			} else {
				
			}
		}

	}
}
