﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorManager : BaseCompent {

	public static UIPanel sprite;
	public static UILabel label;
	public static UISprite sureButton;

	public static void Show(GameObject obj,string error){
		if (sprite == null) {
			sprite = GetGameObjectByName<UIPanel>(obj,"Error Sprite");
			label = GetGameObjectByName<UILabel>(sprite.gameObject,"Label");
			sureButton = GetGameObjectByName<UISprite> (sprite.gameObject, "Sure Button");
			UIEventListener.Get (sureButton.gameObject).onClick += Close;
		}

		JSONObject json = JSONObject.Create (error);

		label.text = json["msg"].str;
		sprite.gameObject.SetActive (true);
	}

	public static void ShowString(GameObject obj,string error){
		ShowText (obj, error);
	}

	private static void ShowText(GameObject obj,string error){
		if (sprite == null) {
			sprite = GetGameObjectByName<UIPanel>(obj,"Error Sprite");
			label = GetGameObjectByName<UILabel>(sprite.gameObject,"Label");
			sureButton = GetGameObjectByName<UISprite> (sprite.gameObject, "Sure Button");
			UIEventListener.Get (sureButton.gameObject).onClick += Close;
		}
		label.text = error;
		sprite.gameObject.SetActive (true);
	}

	public static void Close(GameObject obj){
		sprite.gameObject.SetActive (false);
	}
}
