﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 定义事件分发委托
/// </summary>
public delegate void OnNotification(Notification notific);

/// <summary>
///通知中心 
/// </summary>
public class NotificationCenter
{

	/// <summary>
	/// 通知中心单例
	/// </summary>
	private static NotificationCenter instance=null;
	public static NotificationCenter Get()
	{
		if(instance == null){
			instance = new NotificationCenter();
			return instance;
		}
		return instance;
	}


	/// <summary>
	/// 存储事件的字典
	/// </summary>
	private Dictionary<string,OnNotification> eventListeners = new Dictionary<string, OnNotification>();
//	private Dictionary<string,List<OnNotification>> eventListeners = new Dictionary<string, List<OnNotification>>();

	/// <summary>
	/// 注册事件
	/// </summary>
	///<param name="eventKey">事件Key
	///<param name="eventListener">事件监听器
	public void AddEventListener(string eventKey,OnNotification eventListener)
	{
		
		if (!eventListeners.ContainsKey (eventKey)) {
			eventListeners.Add (eventKey, eventListener);
		}
	}

	/// <summary>
	/// 移除事件
	/// </summary>
	///<param name="eventKey">事件Key
	public void RemoveEventListener(string eventKey,OnNotification eventListener)
	{
		if(!eventListeners.ContainsKey(eventKey))
			return;
		
		eventListeners.Remove (eventKey);


//		eventListeners[eventKey] =null;
//		eventListeners.Remove(eventKey);
	}


	/// <summary>
	/// 分发事件
	/// </summary>
	///<param name="eventKey">事件Key
	///<param name="notific">通知
	public void DispatchEvent(string eventKey,Notification notific)
	{
		if (!eventListeners.ContainsKey(eventKey))
			return;

		eventListeners[eventKey](notific);
	}

	/// <summary>
	/// 分发事件
	/// </summary>
	///<param name="eventKey">事件Key
	///<param name="sender">发送者
	///<param name="param">通知内容
	public void DispatchEvent(string eventKey, GameObject sender, object param)
	{
		if(!eventListeners.ContainsKey(eventKey))
			return;
		eventListeners[eventKey](new Notification(sender,param));
	}

	/// <summary>
	/// 分发事件
	/// </summary>
	///<param name="eventKey">事件Key
	///<param name="param">通知内容
	public void DispatchEvent(string eventKey,object param)
	{
		if(!eventListeners.ContainsKey(eventKey))
			return;
		eventListeners[eventKey](new Notification(param));
	}

	/// <summary>
	/// 是否存在指定事件的监听器
	/// </summary>
	public Boolean HasEventListener(string eventKey)
	{
		return eventListeners.ContainsKey(eventKey);
	}
}



public class Notification
{
	/// <summary>
	/// 通知发送者
	/// </summary>
	public GameObject sender;

	/// <summary>
	/// 通知内容
	/// 备注：在发送消息时需要装箱、解析消息时需要拆箱
	/// 所以这是一个糟糕的设计，需要注意。
	/// </summary>
	public object param;

	/// <summary>
	/// 构造函数
	/// </summary>
	///<param name="sender">通知发送者
	///<param name="param">通知内容
	public Notification(GameObject sender, object param)
	{
		this.sender = sender;
		this.param = param;
	}

	/// <summary>
	/// 构造函数
	/// </summary>
	///<param name="param">
	public Notification(object param)
	{
		this.sender = null;
		this.param = param;
	}


	/// <summary>
	/// 实现ToString方法
	/// </summary>
	/// <returns></returns>
	public override string ToString()
	{
		return this.param.ToString();
	}
}