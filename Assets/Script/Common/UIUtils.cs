﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIUtils : MonoBehaviour {

	public static float duration = 0.2f;

	public static void PanelFadeIn(GameObject gameObject){
		UIPanel panel = gameObject.GetComponent<UIPanel> ();
		panel.gameObject.SetActive (true);
		TweenScale.Begin (panel.gameObject, 0, new Vector3(0,0,0));	
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			TweenScale.Begin (panel.gameObject, duration, new Vector3(1,1,1));	
		}, duration));
	}

	public static void PanelFadeOut(GameObject gameObject){
		UIPanel panel = gameObject.GetComponent<UIPanel> ();
		panel.gameObject.SetActive (true);

		TweenScale.Begin (panel.gameObject,duration, new Vector3(0,0,0));	
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			panel.gameObject.SetActive(false);
		}, duration));
	}
}
