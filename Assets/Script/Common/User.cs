﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum USER_STATUS {
	FREE=0, // "空闲",
	UN_READY=1, // "未准备",
	READY=2, // "已准备",
	WAIT_ACTION=3, // "等待动作",
	CAN_ACTION=4, // "可动作"
	BEFORE_SUBMIT=5, // "正在摆牌"
	AFTER_SUBMIT=6, // "等待其它玩家提交摆牌结果"
	IN_QUEUE = 10, // "匹配队列中",
	BEFORE_QUEUE_TO_ROOM= 11, // "等待确认匹配",
	AFTER__QUEUE_TO_ROOM= 12, // "确认匹配，等待其它玩家确认",
}
	
public class User {

	public string id;
	public string username;
	public string headImgUrl;
	public int cards;
	public int points;
	public int curTotal;
	public int status;
	public int sex;  //1男 0女
	public bool isOnline;
	public bool fake;
	public string code;
	public List<string> cardList = new List<string> ();
	public UISprite pokerContainer;
	public UIPanel resultContainer;

	public User(JSONObject obj,bool isSelf = false){
		this.id = obj ["id"].str;
		this.username = obj ["info"] ["username"].str;
		this.headImgUrl = obj ["info"] ["headimgurl"].str;
		this.cards = obj ["info"] ["cards"] == null?1:int.Parse(obj ["info"] ["cards"].ToString());
		this.points = obj ["info"] ["points"] == null?1:int.Parse(obj ["info"] ["points"].ToString());
		this.curTotal = obj ["curTotal"] == null?0:int.Parse(obj ["curTotal"].ToString());
		this.status = obj ["status"]  == null?0:int.Parse(obj ["status"].ToString());
		this.sex = obj ["sex"] == null ? 0 : int.Parse(obj ["sex"].str);
		this.isOnline = obj["is_online"] == null ? false: bool.Parse(obj ["is_online"].ToString());
		this.fake = obj["info"]["fake"] == null ? false: bool.Parse(obj ["info"]["fake"].ToString());
		this.code = obj ["info"] ["from_share_user"] == null ? "" : obj ["info"] ["from_share_user"].str;

		if (obj ["card_list"] != null) {
			this.cardList.Clear();
			foreach(var c in obj ["card_list"].list){
				this.cardList.Add(c.str);
			}
			if (this.cardList.Count > 0) {
				Utils.SortCard (this.cardList);
			}
		}

		#if UNITY_ANDROID || UNITY_IOS
		if (this.fake && isSelf) {
			this.username = "登录中";
		}
		#endif
	}


	public User(){
		this.id = "";
		this.username = "";
		this.headImgUrl = "";
		this.cards = 0;
		this.points = 0;
		this.status = 0;
	}
}


class UserComparer:IComparer<User>{
	public int Compare(User x, User y) {
		if (x.id == y.id) {
			return 0;
		}
		if (x.id == Main.user.id) {
			return -1;
		}

		if (y.id == Main.user.id) {
			return 1;
		}

		return x.id.CompareTo(y.id);
	}
}
