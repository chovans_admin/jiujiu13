﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnimationUtils{
	private object obj;
	public AnimationUtils(object obj){
		this.obj = obj;
	}
	/*
	public Dictionary<string,object> ani_props = null;

	public void Init(Dictionary<string,object> from_ani){
		this.ani_props = ani;
	}
	public void Begin(Dictionary<string,object> to_ani,float duration){
		foreach (var key in to_ani.Keys)
		{
			var to = to_ani[o];

			switch (key.ToLower())
			{
			case "color":
				TweenColor.Begin(this.obj,duration,(Color)to);
				break;
			case "alpha":
				TweenAlpha.Begin(this.obj,duration,(float)to);
				break;
			case "height":
				TweenHeight.Begin(this.obj,duration,(float)to);
				break;
			case "width":
				TweenWidth.Begin(this.obj,duration,(float)to);
				break;
			case "position":
				TweenPosition.Begin(this.obj,duration,(Vector3)to);
				break;
			case "rotation":
				TweenRotation.Begin(this.obj,duration,(Vector3)to);
				break;
			case "scale":
				TweenScale.Begin(this.obj,duration,(Vector3)to);
				break;
			case "transform":
				TweenTransform.Begin(this.obj,duration,(Transform)to);
				break;
			case "volume":
				TweenVolume.Begin(this.obj,duration,(float)to);
				break;
			default:
				break;
			}
		}
		return new WaitForSeconds(duration);
	}
	public  void To(List<object> args,float duration){
		var to_ani = new Dictionary<string,object>();
		for (int i = 0; i < args.Count; i+=2)
		{
			to_ani.Add((string)args[i], args[i+1]);
		}
		return Begin(to_ani, duration);
	}
	public void ToWithInit(List<object> args,float duration){
		var ani_props = new Dictionary<string,object>();
		var to_ani = new Dictionary<string,object>();
		for (int i = 0; i < args.Count; i+=3)
		{
			ani_props.Add((string)args[i], args[i+1]);
			to_ani.Add((string)args[i], args[i+2]);
		}
		Init(ani_props);
		return Begin(to_ani, duration);
	}
	*/
}
