﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BaseController : MonoBehaviour {

	public static bool isNative = false;


	// Use this for initialization
	void Start () {
	}

	public void callWeiXinLogin(){

		Debug.Log ("调用微信登录");

		#if UNITY_ANDROID
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		jo.Call("login","");
		isNative = true;
		#endif

		#if UNITY_IOS
		SDKTool.Login();
		isNative = true;
		#endif

		#if UNITY_EDITOR 
		API.Login ("weixin");
		isNative = false;
		#endif   
	}

	public void login(string code){
		Debug.Log ("公用Login");
//		API.Login (code);	
		//创建队列
		lock(Main.queues){
			LoginQueue q = new LoginQueue();
			q.time = Convert.ToInt64 ((DateTime.UtcNow - new DateTime (1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
			q.session = code;
			q.isReturn = false;
			q.isRequest = false;

			Main.pushQueue (q);
		}
	}

	public void Update(){
		Main.Update ();
	}

	public static T GetGameObjectByName<T>(GameObject gameObject,string name){
		long begin = DateTime.Now.Ticks;
		GameObject obj = findChild (gameObject, name);
		if (obj != null) {
			//			Debug.Log ("寻找控件:" + name + " 耗时:" + (DateTime.Now.Ticks - begin));
			return obj.GetComponent <T> ();
		}
		return default(T);
	}

	private static GameObject findChild(GameObject parent,string name){
		for (int i = 0; i < parent.transform.childCount; i++) {
			GameObject obj = parent.transform.GetChild(i).gameObject;
			if (obj.name.Equals (name)) {
				return obj;
			}else if (obj.transform.childCount > 0) {
				GameObject child = findChild (obj, name);
				if (child != null) {
					return child;
				}
			}
		}
		return null;
	}
}
