﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P4Controller : MonoBehaviour {

	public List<GameObject> players = new List<GameObject>();

	// Use this for initialization
	void Start () {

		HandlerRoomFrame (null);

		//在接收者中注册事件及其回调方法
//		NotificationCenter.Get().AddEventListener(API.ROOM_ACTION, HandlerRoomFrame);
//		NotificationCenter.Get().AddEventListener(API.USER_ACTION, HandlerRoomExit);
//	}
//
//	void OnDestory(){
//		Debug.Log ("移除监听");
//		NotificationCenter.Get().RemoveEventListener (API.ROOM_ACTION, HandlerRoomFrame);
//		NotificationCenter.Get().RemoveEventListener (API.USER_ACTION, HandlerRoomExit);
	}

	void HandlerRoomFrame(object obj){

		//配置用户信息
		for (int i = 0; i < Main.room.players.Count; i++) {
			PlayerCompent.PlayerInfo (players [i], Main.room.players [i]);
		}
	}

	void HandlerRoomExit(object obj){

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
