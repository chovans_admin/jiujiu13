﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	private static AudioManager instance = null;

	public AudioSource audioMgr;

	private AudioClip ac;
	private string curMusicName = "";

	public static AudioManager Instance
	{
		get
		{ 
			return instance;
		}
	}

	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad(this.gameObject);
	}

	public void  Play(string fileName)
	{
		if (!fileName.Equals(curMusicName))
		{
			ac = Resources.Load("Music/"+fileName) as AudioClip;
			audioMgr.clip = ac;
			audioMgr.Play();
			curMusicName = fileName;
		}
	}

	public void Play(){
		audioMgr.Play();
	}

	public void Pause(){
		audioMgr.Pause ();
	}

	public bool isPlay(){
		return audioMgr.isPlaying;
	}

	public void Stop()
	{
		audioMgr.Stop();
		curMusicName = "";
		Debug.Log("Stop background music");
	}
}
