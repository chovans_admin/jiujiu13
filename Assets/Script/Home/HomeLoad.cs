﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class HomeLoad : BaseController {

	public GameObject rootPanel;

	public GameObject musicPanel;
	public GameObject joinRoomPanel;
	public GameObject shopPanel;
	public UISprite effect;
	public UISprite music;

	public UILabel beanLabel;
	public UILabel masonryLabel;
	public UILabel usernameLabel;
	public UITexture userAvatar;
	public UISprite userAvatarSprite;
	public UILabel idLabel;

	//加入房间
	string num = "";
	public GameObject num1;
	public GameObject num2;
	public GameObject num3;
	public GameObject num4;
	public GameObject num5;
	public GameObject num6;

	//商城
	public GameObject bean;
	public GameObject masonry;
	public GameObject code;
	public GameObject beanPanel;
	public GameObject masonryPanel;
	public GameObject codePanel;
	public GameObject paymentPanel;
	public UIInput codeInput;
	public UIButton codeButton;
	public UILabel codeLabel;


	public GameObject tipSprite;

	//匹配房间
	public GameObject matchPanel;
	public UISprite matchWaiting;
	public UILabel matchCountDown;
	public GameObject matchOpera;

	//消息
	public GameObject messagePanel;
	public UILabel messageLabel;

	//分享
	public GameObject sharePanel;
	public UISprite shareFriend;
	public UISprite shareZone;

	//公告
	public GameObject noticePanel;
	public UIScrollView noticeScrollView;
	public UILabel noticeLabel;

	//意见反馈
	public GameObject feedbackPanel;
	public UIInput feedbackInput;

	//创建房间
	public GameObject createRoomPanel;
	public UISprite closeCreateRoomPanel;

	//联系客服
	public GameObject serverPanel;
	public UISprite closeServerSprite;

	public GameObject errorPanel;
	public UISprite closeErrorSprite;
	// Use this for initialization
	void Start () {
		musicPanel.SetActive (false);
		joinRoomPanel.SetActive (false);
		shopPanel.SetActive (false);
		matchPanel.SetActive (false);
		messagePanel.SetActive (false);
		ReDrawNum ();


		if (PlayerPrefs.GetInt (Main.EFFECT) == 0) {
			effect.spriteName = "sound_close";
		} else {
			effect.spriteName = "sound_open";
		}

		if (PlayerPrefs.GetInt (Main.MUSIC) == 0) {
			music.spriteName = "sound_close";
		} else {
			music.spriteName = "sound_open";
		}


		//加入房间监听
		//添加监听
		NotificationCenter.Get().AddEventListener(API.ROOM_ACTION,JoinRoomResult);
		NotificationCenter.Get().AddEventListener(API.USER_ACTION,RefreshUser);
		NotificationCenter.Get().AddEventListener(API.NOTICE_ACTION,HandlerNotice);
		NotificationCenter.Get().AddEventListener(API.QUEUE_SIZE,HandlerQueue);
		NotificationCenter.Get().AddEventListener(API.ERROR_ACTION,handlerError);
		NotificationCenter.Get().AddEventListener(API.GOODS,HanderGoods);
		NotificationCenter.Get().AddEventListener(API.BUYGOODSSUCCESS,handlerBuySuccess);
		NotificationCenter.Get().AddEventListener(API.BUYGOODSERROR,handlerError);
		NotificationCenter.Get().AddEventListener(API.POINTGOODS,HanderPointGoods);
		NotificationCenter.Get().AddEventListener(API.ROOM_SETTING, HandlerSetting);

		API.GetNotice ();
		//开始滚动字幕
		InvokeRepeating("noticeLoad", 0 , 10);
//		Utils.checkIsInRoom ();
		RefreshUser(null);

		API.GetGoods ();
		API.PointGetGoods ();
		API.GetRoomSetting ();
		//test
//		Debug.Log("角度" + Math.Atan(-1) / Math.PI * 180);
	}

	void RefreshUser (object obj) {  
		//设置水豆和砖石
		beanLabel.text = Main.user.points.ToString();
		masonryLabel.text = Main.user.cards.ToString();
		usernameLabel.text = Main.user.username;
		usernameLabel.depth = 3;
		//更新积分
		UILabel pointLabel = GetGameObjectByName<UILabel>(shopPanel.gameObject,"MyPoint Label");
		if (pointLabel != null) {
//			Debug.Log ("您拥有：" + Main.user.points + " 积分");
			pointLabel.text = "您拥有：" + Main.user.points + " 积分";
		}


		idLabel.text = "ID:"+Main.user.id;
		//加载用户头像
		Main.ImageDownload.SetAsyncImage(Main.user.headImgUrl,userAvatar);

		if (Main.user.code != "") {
			codeLabel.text = "您已绑定邀请码：" + Main.user.code;
			codeInput.gameObject.SetActive (false);
			codeButton.gameObject.SetActive (false);
			codeLabel.gameObject.SetActive (true);
			tipSprite.SetActive (false);
		} else {
			codeInput.gameObject.SetActive (true);
			codeButton.gameObject.SetActive (true);
			codeLabel.gameObject.SetActive (false);
		}


	}  

	void Update(){
		base.Update ();
		UISprite items = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Item Background");
		float width = items.width - 1500;
		for (int i = 0; i < items.transform.childCount; i++) {
			GameObject item = items.transform.GetChild (i).gameObject;
//			
//			TweenPosition.Begin (item.gameObject, 0.001f, new Vector3 (((i * width / 5) + width / 10) - (width / 2), item.transform.position.y, -10));
			item.transform.position = new Vector3(((float)(i * (width / 4)) - (width / 2)) / 360, item.transform.position.y, -10);
		}
	}

	void OnDestroy(){
		Debug.Log ("移除首页监听");
		NotificationCenter.Get ().RemoveEventListener (API.ROOM_ACTION,JoinRoomResult);
		NotificationCenter.Get ().RemoveEventListener (API.NOTICE_ACTION,HandlerNotice);
		NotificationCenter.Get ().RemoveEventListener (API.QUEUE_SIZE,HandlerQueue);
		NotificationCenter.Get().RemoveEventListener(API.ERROR_ACTION,handlerError);
		NotificationCenter.Get().RemoveEventListener(API.BUYGOODSSUCCESS,handlerBuySuccess);
		NotificationCenter.Get().RemoveEventListener(API.BUYGOODSERROR,handlerBuyError);
		NotificationCenter.Get ().RemoveEventListener (API.ROOM_SETTING,HandlerSetting);
	}

	//消息
	public void HandlerSetting(object obj){
		JSONObject json = new JSONObject (obj.ToString ());
		UILabel msgLabel = GetGameObjectByName<UILabel> (rootPanel.gameObject, "Message Label");
		msgLabel.text = json ["home_message"].str;
	}

	// 商城
	public void Shop(){
		EffectUtil.DianJi();
		UIUtils.PanelFadeIn(shopPanel);
		//是否绑定了邀请码
		if (Main.user.code == "") {
			ResetShopClick (code);
			ResetShopPanel (codePanel);
		} else {
			ResetShopClick (masonry);
			ResetShopPanel (masonryPanel);
		}
	}
	public void CloseShop(){
		
		EffectUtil.DianJi();
		UIUtils.PanelFadeOut (shopPanel);
	}
	public void ResetShopPanel(GameObject gameObject){

		if (beanPanel == null)
			beanPanel = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Point Sprite").gameObject;
		beanPanel.SetActive (false);
		if (masonryPanel == null)
			masonryPanel = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Masonry Sprite").gameObject;
		masonryPanel.SetActive (false);
		if (codePanel == null)
			codePanel = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Code Sprite").gameObject;
		codePanel.SetActive (false);
		if (paymentPanel == null)
			paymentPanel = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Payment Sprite").gameObject;
		paymentPanel.SetActive (false);

		if (gameObject == null)
			gameObject = paymentPanel;

		if (Main.user.code == "") {
			codePanel.SetActive (true);
		} else if(gameObject != null){
			gameObject.SetActive (true);
		}
	}
	public void ResetShopClick(GameObject gameObject){
		EffectUtil.DianJi();
		bean.GetComponent<UISprite>().spriteName = "Point";
		masonry.GetComponent<UISprite>().spriteName = "Masonry";
		code.GetComponent<UISprite>().spriteName = "Code";

		UISprite sprite = gameObject.GetComponent<UISprite>();
		if (Main.user.code == "" && sprite.spriteName != "Code" && sprite.spriteName != "Code2") {
			showErrorPanel ("{\"msg\":\"请先绑定邀请码再购物\"}");
			Debug.Log ("没有绑定邀请码，强制回退到输入邀请码界面");
			code.GetComponent<UISprite>().spriteName = "Code2";
		}else if (sprite != null){
			sprite.spriteName = sprite.GetComponent<UISprite>().spriteName + "2";
		}
	}
	public void inviteClick(){
		EffectUtil.DianJi();
		Shop ();
	}

	public void SendCode(){
		if (Main.user.code != null && Main.user.code.Length > 0)
			return;
		EffectUtil.DianJi();
		string code = codeInput.value;
		API.BindCode (code);
	}

	//商城End

	//回调商品
	void HanderGoods(object obj){

		if (rootPanel == null) {
			rootPanel = GameObject.Find ("UI Root");
			Debug.Log ("Find Root:" + rootPanel.name);
		}

		lock (Main.goods) {
			int goodsIdx = 0;
			int line = (int)Math.Ceiling((double)Main.goods.Count/3d);
			UISprite grid = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Masonry Grid");
			grid.depth = 7;
			for (int i = 0; i < line; i++) {
				GameObject gameObject = (GameObject)(Resources.Load("Home/Goods Items"));
				GameObject item = NGUITools.AddChild(grid.gameObject , gameObject);
				for (int j = 0; j < 3; j++) {
					int idx = goodsIdx % 3 + 1;
					GameObject subItem = GetGameObjectByName<UITexture> (item,"item"+idx).gameObject;
					subItem.SetActive (true);

					UITexture texture = subItem.GetComponent<UITexture> ();
					//商品图片
					try{
						Main.ImageDownload.SetAsyncImage ( Main.goods[goodsIdx]["coverimageurl"].str,texture);
//						Main.ImageDownload.SetAsyncImage("http://o8epcn0o9.bkt.clouddn.com/2c9092815996e837015997027f4b00001484297311196",texture);
					}catch(Exception e){
						Debug.Log (e.Message);
					}
					UILabel label = GetGameObjectByName<UILabel> (subItem, "Label");
					label.text = "￥" + Main.goods [goodsIdx] ["price"].ToString ();

					UISprite button = GetGameObjectByName<UISprite> (subItem, "Button");
					UIEventListener.Get (button.gameObject).Clear ();
					UIEventListener.Get (button.gameObject).onClick += BuyGoodsClick;

					//7-9
					UILabel num = GetGameObjectByName<UILabel>(subItem,"Num");
					num.text = Main.goods [goodsIdx] ["get_cards"].ToString ();
					num.fontSize = 200;
					goodsIdx++;
				}
			}
			UIScrollView scrollView = GetGameObjectByName<UIScrollView> (rootPanel.gameObject, "Masonry Scroll View");
			GetGameObjectByName<UIScrollBar> (rootPanel.gameObject, "Masonry Scroll Bar").value = 1f;	
			grid.GetComponent<UIGrid>().repositionNow = true;
			scrollView.ResetPosition ();
		}

	}

	void BuyGoodsClick(GameObject gameObject){

		EffectUtil.DianJi();

		UILabel label = GetGameObjectByName<UILabel> (gameObject, "Label");
		string id = null;
		foreach(var g in Main.goods){
			if (g ["price"].ToString () == label.text.Replace("￥","")) {
				id = g["id"].str;
			}
		}
//		API.BuyGoods (id);
		SelectPayment(id);
	}

	//购买成功
	void handlerBuySuccess(object obj){
		JSONObject json = new JSONObject (obj.ToString ());

		if (json["pay_params"]["appId"] == null) {
			#if UNITY_ANDROID
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
			jo.Call("aliPay",json ["pay_params"].str);
			#endif

			#if UNITY_IOS
			SDKTool.AliPay (json ["pay_params"].str);
			#endif	
		} else {
			
			string appid = json["pay_params"]["appId"].str;
			string partnerId = json["pay_params"]["partnerId"].str;
			string prepayId = json["pay_params"]["prepayId"].str;
			string nonceStr = json["pay_params"]["nonceStr"].str;
			string timeStamp = json["pay_params"]["timeStamp"].str;
			string sign = json["pay_params"]["sign"].str;

			#if UNITY_ANDROID
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
			jo.Call("wxPay",appid+"|"+partnerId+"|"+prepayId+"|"+nonceStr+"|"+timeStamp+"|"+sign);
			#endif

			#if UNITY_IOS
			SDKTool.WxPay (appid+"|"+partnerId+"|"+prepayId+"|"+nonceStr+"|"+timeStamp+"|"+sign);
			#endif	
		}
			
		UILabel waitLabel = GetGameObjectByName<UILabel> (rootPanel.gameObject,"Buy Wait");
		waitLabel.gameObject.SetActive (false);
	}

	//购买失败
	void handlerBuyError(object obj){
//		HomeLoad.handlerError (obj.ToString());
		ErrorManager.Show(rootPanel,obj.ToString());
		UILabel waitLabel = GetGameObjectByName<UILabel> (rootPanel.gameObject,"Buy Wait");
		waitLabel.gameObject.SetActive (false);
	}

	//回调积分商品
	void HanderPointGoods(object obj){
			
		if (rootPanel == null) {
			rootPanel = GameObject.Find ("UI Root");
			Debug.Log ("Find Root:" + rootPanel.name);
		}

		lock (Main.pointGoods) {
			int goodsIdx = 0;
			int line = (int)Math.Ceiling((double)Main.pointGoods.Count/3d);
			UISprite grid = GetGameObjectByName<UISprite> (rootPanel.gameObject, "Point Grid");
			grid.depth = 7;
			for (int i = 0; i < line; i++) {
				GameObject gameObject = (GameObject)(Resources.Load("Home/Point Items"));
				GameObject item = NGUITools.AddChild(grid.gameObject , gameObject);
				for (int j = 0; j < 3; j++) {

					if (goodsIdx >= Main.pointGoods.Count) {
						return;
					}
					int idx = goodsIdx % 3 + 1;
					GameObject subItem = GetGameObjectByName<UITexture> (item,"item"+idx).gameObject;
					subItem.SetActive (true);

					UITexture texture = subItem.GetComponent<UITexture> ();
					//商品图片
					try{
						Main.ImageDownload.SetAsyncImage ( Main.pointGoods[goodsIdx]["coverimageurl"].str,texture);
						//					Main.ImageDownload.SetAsyncImage("http://o8epcn0o9.bkt.clouddn.com/2c9092815996e837015997027f4b00001484297311196",texture);
					}catch(Exception e){
						Debug.Log (e.Message);
					}
					UILabel label = GetGameObjectByName<UILabel> (subItem, "Label");
					label.text = Main.pointGoods [goodsIdx] ["cut_points"].ToString ();

					UISprite button = GetGameObjectByName<UISprite> (subItem, "Button");
					UIEventListener.Get (button.gameObject).Clear ();
					UIEventListener.Get (button.gameObject).onClick += BuyPointGoodsClick;

					//7-9
					UILabel num = GetGameObjectByName<UILabel>(subItem,"Num");
					num.text = Main.pointGoods [goodsIdx] ["add_cards"].ToString ();
					num.fontSize = 200;

					goodsIdx++;
				}
			}
			GetGameObjectByName<UIScrollView> (rootPanel.gameObject, "Point Scroll View").ResetPosition ();
			UIScrollView scrollView = GetGameObjectByName<UIScrollView> (rootPanel.gameObject, "Point Scroll View");
			gameObject.GetComponent<UIDragScrollView> ().scrollView = scrollView;
			GetGameObjectByName<UIScrollBar> (rootPanel.gameObject, "Point Scroll Bar").value = 1f;
			grid.GetComponent<UIGrid>().repositionNow = true;
			scrollView.ResetPosition ();
		}
	}

	void BuyPointGoodsClick(GameObject gameObject){

		EffectUtil.DianJi();

		UILabel label = GetGameObjectByName<UILabel> (gameObject, "Label");
		string id = null;
		foreach(var g in Main.pointGoods){
			if (g ["cut_points"].ToString () == label.text.Replace("￥","")) {
				id = g["id"].str;
			}
		}
//		API.BuyGoods (id);
//		SelectPayment(id);
		goodsId = id;
		Buy(null);
	}

	private string goodsId = null;
	private string payment = null;
	void SelectPayment(string id){
		goodsId = id;
		ResetShopPanel (paymentPanel);
		UISprite alipay = GetGameObjectByName<UISprite> (paymentPanel.gameObject, "Alipay");
		UISprite wxpay = GetGameObjectByName<UISprite> (paymentPanel.gameObject, "Wxpay");

		UIEventListener.Get (alipay.gameObject).Clear ();
		UIEventListener.Get (wxpay.gameObject).Clear ();

		UIEventListener.Get (alipay.gameObject).onClick += Buy;
		UIEventListener.Get (wxpay.gameObject).onClick += Buy;
	}


	void Buy(GameObject gameObject){
		EffectUtil.DianJi ();

		//判断支付方式
		if (gameObject != null) {
			payment = gameObject.GetComponent<UISprite> ().spriteName ;
			API.BuyGoods (goodsId, payment == "AliPay" ? true : false);
		} else {
			API.BuyPointGoods (goodsId);
		}
			
		UILabel waitLabel = GetGameObjectByName<UILabel> (rootPanel.gameObject,"Buy Wait");
		waitLabel.gameObject.SetActive (true);
	}

	void HandlerQueue(object obj){
		JSONObject json = new JSONObject (obj.ToString());
		int waitNum = int.Parse(json["size"].ToString());
		Debug.Log ("确定人数："+waitNum);
		if (waitNum < 5) {
			matchCountDown.alpha = 0;
			matchWaiting.spriteName = "Match Wait " + waitNum;
			matchOpera.GetComponent<UISprite> ().spriteName = "Cancel";
		} 
		if(waitNum == 4){
			waitTime = 10;
			CancelInvoke ();
			InvokeRepeating ("WaitConfirm", 0, 1);
		}
	}

	int waitTime = 10;
	public void WaitConfirm(){
		waitTime--;
		matchOpera.GetComponent<UISprite> ().spriteName = "Sure";
		matchCountDown.alpha = 1;
		matchCountDown.text = waitTime.ToString();
		if (waitTime == -1) {
			CancelInvoke ();
			API.CancelConfirm ();
			UIUtils.PanelFadeOut (matchPanel.gameObject);
		}
		/*
		while (waitTime > -1 || waitTime < 11) {
			//需要重复执行的代码就放于在此处
			matchCountDown.text = waitTime.ToString();
			yield return new WaitForSeconds(1f);
			waitTime--;
			//如果超过时间，退出匹配队列
			if (waitTime == -1) {
				API.CancelConfirm ();
				UIUtils.PanelFadeOut (matchPanel.gameObject);
			}

		};*/

		if (waitTime > 11) {
			matchOpera.SetActive (false);
		} else {
			matchOpera.SetActive (true);
		}
	}

	// 等待匹配
	public void Match(){

		//加入匹配队列
		API.JoinQueue ();

		UIUtils.PanelFadeIn (matchPanel.gameObject);
//		matchPanel.SetActive (true);
		matchCountDown.alpha = 0;
		matchCountDown.text = "10";
	}

	public void CancelMatch(){
		if (matchOpera.GetComponent<UISprite> ().spriteName == "Cancel") {
			//取消排队
			API.CancelConfirm ();
			UIUtils.PanelFadeOut (matchPanel.gameObject);
		} else {
			CancelInvoke ();
			API.QueueComfirm ();
			matchCountDown.text = "等待确认";
		}
//		matchPanel.SetActive (false);
	}



	// ###music
	public void Music(){
		EffectUtil.DianJi();
//		musicPanel.SetActive (true);
		UIUtils.PanelFadeIn(musicPanel);
	}
	public void CloseMusic(){
		EffectUtil.DianJi();
//		musicPanel.SetActive (false);
		UIUtils.PanelFadeOut(musicPanel);
	}

	public void EffectSwitch(){
		EffectUtil.DianJi();
		PlayerPrefs.SetInt (Main.EFFECT,PlayerPrefs.GetInt (Main.EFFECT) == 0 ? 1 : 0);
		EffectToggle ();
	}
	public void MusicSwitch(){
		EffectUtil.DianJi();
		PlayerPrefs.SetInt (Main.MUSIC,PlayerPrefs.GetInt (Main.MUSIC) == 0 ? 1 : 0);
		MusicToggle ();
	}

	public void EffectToggle(){
		if (PlayerPrefs.GetInt (Main.EFFECT) == 0) {
			effect.spriteName = "sound_close";
		} else {
			effect.spriteName = "sound_open";
		}
	}
	public void MusicToggle(){
		if (PlayerPrefs.GetInt (Main.MUSIC) == 0) {
			music.spriteName = "sound_close";
			AudioManager.Instance.Pause ();
		} else {
			music.spriteName = "sound_open";
			AudioManager.Instance.Play ();
		}
	}

	// ####music end

	// 创建房间
	public void CreateRoom(){
		SceneManager.LoadScene ("CreateRoom");
	}
	// 创建房间End

	//加入房间
	public void JoinRoom(){
		EffectUtil.DianJi();
//		joinRoomPanel.SetActive (true);
		UIUtils.PanelFadeIn(joinRoomPanel);
	}

	public void CloseJoinRoom(){
		EffectUtil.DianJi();
//		joinRoomPanel.SetActive (false);
		UIUtils.PanelFadeOut(joinRoomPanel);
	}

	public void Clear(){
		num = "";
		ReDrawNum ();
	}

	public void Del(){
		if (num.Length < 2) {
			num = "";
		} else {
			num = num.Substring (0, num.Length - 1);
		}

		ReDrawNum ();
	}

	public void Add(GameObject n){
		UILabel label = n.GetComponent<UILabel> ();
		if (num.Length < 6) {
			num = num + label.name;	
		}

		//FIXME 6位房间号
		if (num.Length >= 6) {
			API.JoinRoom (num);
			Debug.Log ("开始查找房间");
		}

		ReDrawNum ();
	}

	void JoinRoomResult(object obj){
//		Utils.checkRoomInfo (obj);
	}


	void ReDrawNum(){
		if (num.Length > 0) {
			num1.GetComponent<UILabel> ().text = num [0].ToString ();	
		} else {
			num1.GetComponent<UILabel> ().text = "";
		}
		if (num.Length > 1) {
			num2.GetComponent<UILabel> ().text = num [1].ToString ();	
		} else {
			num2.GetComponent<UILabel> ().text = "";
		}
		if (num.Length > 2) {
			num3.GetComponent<UILabel> ().text = num [2].ToString ();	
		}else
			num3.GetComponent<UILabel> ().text = "";
		if (num.Length > 3) {
			num4.GetComponent<UILabel> ().text = num [3].ToString ();	
		}else
			num4.GetComponent<UILabel> ().text = "";
		if (num.Length > 4) {
			num5.GetComponent<UILabel> ().text = num [4].ToString ();	
		}else
			num5.GetComponent<UILabel> ().text = "";
		if (num.Length > 5) {
			num6.GetComponent<UILabel> ().text = num [5].ToString ();	
		}else
			num6.GetComponent<UILabel> ().text = "";
	}

	//加入房间End


	//查看战绩
	public void GameResult(){
		EffectUtil.DianJi();
		SceneManager.LoadScene ("Result");
	}

	//查看玩法
	public void Export(){
		EffectUtil.DianJi();
		SceneManager.LoadScene ("Export");
	}

	//消息
	public void ShowMessage(){
		EffectUtil.DianJi();
//		messagePanel.SetActive (true);
		UIUtils.PanelFadeIn(messagePanel);
	}

	public void HideMessage(){
		EffectUtil.DianJi();
//		messagePanel.SetActive (false);
		UIUtils.PanelFadeOut(messagePanel);
	}

	public void HandlerNotice(object obj){
		if (Main.notice == null) {
			JSONObject json = new JSONObject (obj.ToString ());
			Main.notice = json ["bulletin"].str;
		}
		noticeLabel.text = Main.notice;
	}

	private void noticeLoad(){
		//设置初始位置
//		float width = noticeScrollView.bounds.max.x - noticeScrollView.bounds.min.x;
		noticeLabel.text = Main.notice;
		float labelWidth = noticeLabel.CalculateBounds ().max.x - noticeLabel.CalculateBounds ().min.x;
		noticeLabel.gameObject.SetActive (false);
		TweenPosition.Begin (noticeLabel.gameObject, 0.01f, new Vector3(2800f,noticeLabel.transform.position.y,-10));
		noticeLabel.gameObject.SetActive (true);
		//滚动设置
		StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
			{
				TweenPosition.Begin (noticeLabel.gameObject, 10f, new Vector3(-2800-labelWidth,noticeLabel.transform.position.y,-10));
			}, (1f)));
		
	}

	//分享
	public void showShare(){
		EffectUtil.DianJi();
		UIUtils.PanelFadeIn(sharePanel);
//		sharePanel.SetActive (true);
	}

	public void hideShare(){
		EffectUtil.DianJi();
		UIUtils.PanelFadeOut(sharePanel);
//		sharePanel.SetActive (false);
	}

	//分享给好友
	public void ShareFriend(){
		EffectUtil.DianJi();
		Debug.Log ("分享给好友");
		#if UNITY_ANDROID
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		jo.Call("shareFriend","");
		#endif

		#if UNITY_IOS
		SDKTool.ShareFriend();
		#endif
	}
	public void ShareZone(){
		EffectUtil.DianJi();
		Debug.Log ("分享到朋友圈");
		#if UNITY_ANDROID
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		jo.Call("shareZone","");
		#endif

		#if UNITY_IOS
		SDKTool.ShareZone();
		#endif
	}

	//意见反馈
	public void showFeedback(){
		EffectUtil.DianJi();
		UIUtils.PanelFadeIn (feedbackPanel);
	}
	public void hideFeedback(){
		EffectUtil.DianJi();
		UIUtils.PanelFadeOut (feedbackPanel);
	}
	public void SubmitFeedback(){
		EffectUtil.DianJi();
		string value = feedbackInput.value;
		Debug.Log(value);
	}

	//创建房间面板
	public void showCreateRoomPanel(){
		EffectUtil.DianJi();
		createRoomPanel.SetActive (true);
	}
	public void hideCreateRoomPanel(){
		EffectUtil.DianJi();
		createRoomPanel.SetActive (false);
	}

	//lianxikefu
	public void showServerPanel(){
		serverPanel.SetActive (true);
	}
	public void hideServerPanel(){
		serverPanel.SetActive (false);
	}

	//出错提醒
	public void showErrorPanel(string obj){
		UIUtils.PanelFadeIn (errorPanel.gameObject);
		ErrorManager.Show(rootPanel,obj);
	}

	public void hideErrorPanel(){
		UIUtils.PanelFadeOut (errorPanel.gameObject);
	}

	public void handlerError(object obj){
		EffectUtil.DianJi();
		showErrorPanel (obj.ToString());
	}

}
