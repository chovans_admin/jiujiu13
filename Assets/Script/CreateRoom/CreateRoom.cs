﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CreateRoom : BaseCompent {

	public UIPanel rootPanel;
	public UIPanel leftPanel;

	//支付方式
	public GameObject payment1;
	public GameObject payment2;
	//局数
	public GameObject gameNum1;
	public GameObject gameNum2;
	public GameObject gameNum3;
	public GameObject gameNum4;

	public GameObject commonPanel;
	public GameObject PtPanel;
	public GameObject JysPanel;
	public GameObject LzPanel;
	public GameObject P5Panel;
	public GameObject P6Panel;
	public GameObject TwPanel;

	// #####普通场
	public GameObject ptPersonNum1;
	public GameObject ptPersonNum2;
	public GameObject ptPersonNum3;

	// ######加一色
	public GameObject jysPersionNum1;
	public GameObject jysPersionNum2;
	public GameObject jysPersionNum3;

	public GameObject jysHuaSeNum1;
	public GameObject jysHuaSeNum2;
	public GameObject jysHuaSeNum3;
	public GameObject jysHuaSeNum4;

	// #########癞子
	public GameObject lzPersonNum1;
	public GameObject lzPersonNum2;
	public GameObject lzPersonNum3;

	public GameObject lzIsShow1;
	public GameObject lzIsShow2;

	// #########5人场
	public GameObject p5PersonNum1;
	public GameObject p5PersonNum2;

	public GameObject p5HuaSeNum1;
	public GameObject p5HuaSeNum2;
	public GameObject p5HuaSeNum3;
	public GameObject p5HuaSeNum4;
	public GameObject p5HuaSeLabel;
	public GameObject p5HuaSeNum5;
	public GameObject p5HuaSeNum6;
	public GameObject p5HuaSeNum7;
	public GameObject p5HuaSeNum8;

	// ###########6人场
	public GameObject p6HuaSe1Num1;
	public GameObject p6HuaSe1Num2;
	public GameObject p6HuaSe1Num3;
	public GameObject p6HuaSe1Num4;

	public GameObject p6HuaSe2Num1;
	public GameObject p6HuaSe2Num2;
	public GameObject p6HuaSe2Num3;
	public GameObject p6HuaSe2Num4;


	// ###########加3张加四张
	public GameObject PersonNum2;
	public GameObject PersonNum3;
	public GameObject PersonNum4;
	public GameObject PersonNum5;
	public GameObject PersonNum6;

	public GameObject PokerNum3;
	public GameObject PokerNum4;

	public UILabel label;

	// 系统参数
	public int _playerNum = 2;
	public bool _withLaizi = false;
	public int _maxRound = 5;
	public bool _isShow = false;
	public string _extendColor1 = "";
	public string _extendColor2 = "";
	public string _payment = API.CREATE_ROOM_PARAM.Payment_Master;
	public string _mode = "消费建房";//自由匹配
	public int _type = 0;//type  0:普通,1:加一色,2:癞子,3:5人场,4:6人场,5:加3、4张
	public int _pokers = 0;

	public void Start(){
		resetButton ("PT Button");
		SetPanelDefault (PtPanel);
		//设置默认值
		PaymentSwitchClick(payment1);
		GameNumSwitchClick (gameNum2);
		PtNumSwitchCLick (ptPersonNum1);


		NotificationCenter.Get().AddEventListener(API.PAY_PRICE,PriceHandler);
	}

	void OnDestroy(){
		NotificationCenter.Get ().RemoveEventListener (API.ROOM_ACTION,PriceHandler);
	}

	private void PriceHandler(object obj){
		JSONObject price = new JSONObject (obj.ToString());
		label.text = "该房间消耗砖石"+ price["price"].ToString() +"颗";
	}

//	void Update(){
//		float height = leftPanel.height;
//		for (int i = 0; i < leftPanel.transform.childCount; i++) {
//			float itemHeight = itemWidth / 2;
//			float x = (width / 12 * (1 + (i % 2 == 0 ? 0 : 3))) - (width/1.5f);
//			float y = height / 6 * (i < 2 ? 1 : -1) - (height / 5);
//			UIWidget g = leftPanel.transform.GetChild (i).gameObject.GetComponent<UIWidget> ();
//
//			g.width = (int)(rootPanel.width / 1.5);
//			g.height = (int)itemHeight;
//			g.transform.position = new Vector3 (x / 100, y / 100, g.transform.position.z);
//		}
//	}

	public void PanelButtonClick(GameObject gameObject){
		
		SetPanelDefault (gameObject);
		string name = gameObject.GetComponent<UIPanel>().name;
		_withLaizi = false;
		if (name.Contains ("Pt")) {
			_type = 0;
			PtNumSwitchCLick (ptPersonNum1);
		} else if (name.Contains ("J")) {
			_type = 1;
			JysNumSwitchClick (jysPersionNum1);
			JysHuaSeSwitchClick (jysHuaSeNum1);
		} else if (name.Contains ("L")) {
			_type = 2;
			LzIsShowSwitch (lzIsShow1);
			LzNumSwitch (lzPersonNum1);
			_withLaizi = true;

			//癞子改成敬请期待
			commonPanel.SetActive (false);

		} else if (name.Contains ("P5")) {
			_type = 3;
			p5NumSwitch (p5PersonNum1);
			p5HuaSeSwitch (p5HuaSeNum1);
			_playerNum = 5;
		} else if (name.Contains ("P6")) {
			_type = 4;
			p6HuaSe1Switch (p6HuaSe1Num1);
			p6HuaSe2Switch (p6HuaSe2Num1);
			//人数
			_playerNum = 6;
		} else if (name.Contains ("Tw")) {
			_type = 5;
			jiaNumSwitch (PersonNum2);
			jiaPokerSwitch (PokerNum3);
			_pokers = 3;
		}
		PaymentSwitchClick (payment1);
		GameNumSwitchClick (gameNum2);

		resetButton (gameObject.name.Split (' ') [0] + " Button");


	}

	private void resetButton(string buttonName){
		string[] bts = new string[]{"PT Button","JYS Button","LZ Button","P5 Button","P6 Button","Tw Button"};
		foreach (var bt in bts) {
			UIButton button = GetGameObjectByName<UIButton> (leftPanel.gameObject, bt);
		
			button.GetComponent<UIButton>().normalSprite = "Register Btn";
		}
		UIButton currButton = GetGameObjectByName<UIButton> (leftPanel.gameObject, buttonName);
		currButton.normalSprite = "Register Btn3";
	}

	//创建房间或获取房费
	public void CreateRoomSend(bool isCreate){
		if (_type == 0) {
			_extendColor1 = "";
			_extendColor2 = "";
		}
		if (_type != 4) {
			_extendColor2 = "";
		}
		if (_type == 4 && _extendColor1.Equals (_extendColor2) && isCreate) {
			NotificationCenter.Get ().DispatchEvent (API.ERROR_ACTION, "{\"msg\":\"花色不能相同\"}");
			return;
		} 
		API.CreateRoom (_playerNum,_withLaizi,_maxRound,_extendColor1,_extendColor2,_payment,_mode,_isShow,_type,_pokers,isCreate);	
	}

	//获取房费
	private void GetPrice(){
		CreateRoomSend (false);
	}

	//创建房间
	private void CreateRoomClick(){
		CreateRoomSend (true);
	}

	//普通场
	public void PtNumSwitchCLick(GameObject button){
		
		//人数
		string name = button.GetComponent<UIButton>().name;
		_playerNum = int.Parse(RemoveNotNumber(name));

		setDefault (new GameObject[]{ptPersonNum1,ptPersonNum2,ptPersonNum3 });
		setSwitch (button);
	}
	//加一色
	public void JysNumSwitchClick(GameObject button){
		
		//人数
		string name = button.GetComponent<UIButton>().name;
		_playerNum = int.Parse(RemoveNotNumber(name));

		setDefault (new GameObject[]{jysPersionNum1,jysPersionNum2,jysPersionNum3 });
		setSwitch (button);
	}

	public void JysHuaSeSwitchClick(GameObject button){
		//花色
		string name = button.GetComponent<UIButton>().name;
		_extendColor1 = getHuaSe (name);

		setDefault (new GameObject[]{jysHuaSeNum1,jysHuaSeNum2,jysHuaSeNum3,jysHuaSeNum4 });
		setSwitch (button);
	}

	//癞子
	public void LzNumSwitch(GameObject button){
		//人数
		string name = button.GetComponent<UIButton>().name;
		_playerNum = int.Parse(RemoveNotNumber(name));

		setDefault (new GameObject[]{lzPersonNum1,lzPersonNum2,lzPersonNum3 });
		setSwitch (button);
	}

	public void LzIsShowSwitch(GameObject button){
		//是否显示余牌
		string name = button.GetComponent<UIButton>().name;
		if (name.Contains ("0")) {
			_isShow = false;
		} else {
			_isShow = true;
		}
			
		setDefault (new GameObject[]{lzIsShow1,lzIsShow2 });
		setSwitch (button);
	}

	//5人场
	public void p5NumSwitch(GameObject button){
		
		//人数
		string name = button.GetComponent<UIButton>().name;
		_playerNum = int.Parse(RemoveNotNumber(name)) == 51 ? 5 : 6;

		GameObject[] huase2 = new GameObject[]{ p5HuaSeLabel,p5HuaSeNum5, p5HuaSeNum6, p5HuaSeNum7, p5HuaSeNum8 };
		foreach (var g in huase2) {
			if (_playerNum == 5) {
				_type = 3;
				g.SetActive (false);
			}
			else{
				_type = 4;
				g.SetActive (true);
				p5HuaSe2Switch (p5HuaSeNum5);
			}
		}

		setDefault (new GameObject[]{ p5PersonNum1, p5PersonNum2 });
		setSwitch (button);
	}

	public void p5HuaSeSwitch(GameObject button){
		
		//花色
		string name = button.GetComponent<UIButton>().name;
		_extendColor1 = getHuaSe (name);

		setDefault (new GameObject[]{p5HuaSeNum1,p5HuaSeNum2,p5HuaSeNum3,p5HuaSeNum4 });
		setSwitch (button);
	}

	public void p5HuaSe2Switch(GameObject button){
		
		//花色
		string name = button.GetComponent<UIButton>().name;
		_extendColor2 = getHuaSe (name);

		setDefault (new GameObject[]{p5HuaSeNum5,p5HuaSeNum6,p5HuaSeNum7,p5HuaSeNum8 });
		setSwitch (button);
	}
	//6人场
	public void p6HuaSe1Switch(GameObject button){
		
		//花色1
		string name = button.GetComponent<UIButton>().name;
		_extendColor1 = getHuaSe (name);

		setDefault (new GameObject[]{p6HuaSe1Num1,p6HuaSe1Num2,p6HuaSe1Num3,p6HuaSe1Num4});
		setSwitch (button);
	}
	public void p6HuaSe2Switch(GameObject button){
		
		//花色2
		string name = button.GetComponent<UIButton>().name;
		_extendColor2 = getHuaSe (name);

		setDefault (new GameObject[]{p6HuaSe2Num1,p6HuaSe2Num2,p6HuaSe2Num3,p6HuaSe2Num4});
		setSwitch (button);
	}

	//加3张加4张
	//人数
	public void jiaNumSwitch(GameObject button){
		
		string name = button.GetComponent<UIButton>().name;
		_playerNum = int.Parse(RemoveNotNumber(name));

		setDefault (new GameObject[]{PersonNum2,PersonNum3,PersonNum4,PersonNum5,PersonNum6});
		setSwitch (button);
	}

	public void jiaPokerSwitch(GameObject button){
		
		string name = button.GetComponent<UIButton>().name;
		_pokers = int.Parse (RemoveNotNumber (name));

		setDefault (new GameObject[]{PokerNum4,PokerNum3});
		setSwitch (button);
	}

	//Common
	public void PaymentSwitchClick(GameObject button){
		//_payment
		if (button.GetComponent<UIButton>().name.Contains ("Master")) {
			_payment = API.CREATE_ROOM_PARAM.Payment_Master;
		}else{
			_payment = API.CREATE_ROOM_PARAM.Payment_Flat;
		}
		Debug.Log (_payment);
		setDefault (new GameObject[]{payment1,payment2 });
		setSwitch (button);

	}

	public void GameNumSwitchClick(GameObject button){
		//_maxRound
		string name = button.GetComponent<UIButton>().name;
		_maxRound = int.Parse(RemoveNotNumber(name));
		Debug.Log (_maxRound);

		setDefault (new GameObject[]{gameNum1,gameNum2,gameNum3,gameNum4 });
		setSwitch (button);
	}

	public void ExitClick(){
		SceneManager.LoadScene ("Home");
	}

	private void SetPanelDefault(GameObject gameObject){
		commonPanel.SetActive(true);
		PtPanel.SetActive (false);
		JysPanel.SetActive (false);
		LzPanel.SetActive (false);
		P5Panel.SetActive (false);
		P6Panel.SetActive (false);
		TwPanel.SetActive (false);
		gameObject.SetActive (true);
	}

	private void setDefault(GameObject[] gameObjects){
		for (int i = 0; i < gameObjects.Length; i++) {
			gameObjects[i].GetComponent<UIButton>().normalSprite = "Check False";
		}
	}
	private void setSwitch(GameObject gameObject){
		gameObject.GetComponent<UIButton>().normalSprite = "Check Ture";
		//检测房费
		GetPrice ();
	}

	private string RemoveNotNumber(string key)  
	{  
		return  Regex.Replace(key, @"[^\d]*", "");
	}
	private string getHuaSe(string key){
		string huase = "未知花色";
		if (key.Contains ("HET")) {
			huase = "黑桃*";
		} else if (key.Contains ("HOT")) {
			huase = "红心*";
		} else if (key.Contains ("MH")) {
			huase = "梅花*";
		} else if (key.Contains ("FK")) {
			huase = "方块*";
		}
//		Debug.Log ("花色："+huase);
		return huase;
	}
}
