﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//控制类
public class ControllerCompent : BaseCompent {

	public static  UISprite pullDown = null;
	public static  UISprite pullUp = null;
	public static UISprite background = null;
	public static UISprite leave = null;

	public static void ControllerInit(UIPanel panel){
		pullDown = GetGameObjectByName<UISprite> (panel.gameObject, "Pull Down");
		pullUp = GetGameObjectByName<UISprite> (panel.gameObject, "Pull Up");
		background = GetGameObjectByName<UISprite> (panel.gameObject, "Background");
		leave = GetGameObjectByName<UISprite> (panel.gameObject, "Leave");

		if (PlayerPrefs.GetInt (Main.EFFECT) == 1) {
			GetGameObjectByName<UISprite> (panel.gameObject, "Effect").spriteName = "Effect On";
		} else {
			GetGameObjectByName<UISprite> (panel.gameObject, "Effect").spriteName = "Effect Off";
		}

		if (PlayerPrefs.GetInt (Main.MUSIC) == 1) {
			GetGameObjectByName<UISprite> (panel.gameObject, "Music").spriteName = "Music On";
		} else {
			GetGameObjectByName<UISprite> (panel.gameObject, "Music").spriteName = "Music Off";
		}

		UIEventListener.Get (pullDown.gameObject).onClick += IsShow;
		UIEventListener.Get (pullUp.gameObject).onClick += IsShow;
		UIEventListener.Get (leave.gameObject).onClick += Leave;

		//isshow会改变状态
		background.gameObject.SetActive (true);
		IsShow (null);

		UIEventListener.Get (GetGameObjectByName<UISprite> (panel.gameObject, "Music").gameObject).onClick += Music;
		UIEventListener.Get (GetGameObjectByName<UISprite> (panel.gameObject, "Effect").gameObject).onClick += Effect;
		UIEventListener.Get (GetGameObjectByName<UISprite> (panel.gameObject, "Leave").gameObject).onClick += Leave;
	}

	//监控情况
	public static void HandlerStatus(){
		if (Main.room.payMethod == "房主支付"
			&& Main.room.owner != null  && Main.room.owner.id == Main.user.id
		    && Main.room.status == ROOM_STATUS.UN_READY.GetHashCode ()) {
			leave.spriteName = "Dissolution";
		} else if(leave != null){
			leave.spriteName = "Leave";
		}
	}

	private static void IsShow(GameObject gameObject){
//		if (background.gameObject.activeInHierarchy) {
//			TweenAlpha.Begin (background.gameObject, 0.3f, 0);	
//		} else {
//			TweenAlpha.Begin (background.gameObject, 0.3f, 1);	
//		}
		if(gameObject != null)
			EffectUtil.DianJi();
		background.gameObject.SetActive (!background.gameObject.activeInHierarchy);

		if (!background.gameObject.activeInHierarchy) {
			pullDown.gameObject.SetActive (true);
		} else {
			pullDown.gameObject.SetActive (false);
		}
	}

	private static void Effect(GameObject gameObject){
		PlayerPrefs.SetInt (Main.EFFECT,PlayerPrefs.GetInt (Main.EFFECT) == 0 ? 1 : 0);

		if (PlayerPrefs.GetInt (Main.EFFECT) == 1) {
			gameObject.GetComponent<UISprite> ().spriteName = "Effect On";
		} else {
			gameObject.GetComponent<UISprite> ().spriteName = "Effect Off";
		}

//		PlayerPrefs.SetInt (Main.EFFECT,PlayerPrefs.GetInt (Main.EFFECT) == 0 ? 1 : 0);
	}

	private static void Music(GameObject gameObject){
		PlayerPrefs.SetInt (Main.MUSIC,PlayerPrefs.GetInt (Main.MUSIC) == 0 ? 1 : 0);

		if (PlayerPrefs.GetInt (Main.MUSIC) == 1) {
			if (!AudioManager.Instance.isPlay()) {
				AudioManager.Instance.Play ();
			}
			gameObject.GetComponent<UISprite> ().spriteName = "Music On";
		} else {
			AudioManager.Instance.Pause ();
			gameObject.GetComponent<UISprite> ().spriteName = "Music Off";
		}

//		PlayerPrefs.SetInt (Main.MUSIC,PlayerPrefs.GetInt (Main.MUSIC) == 0 ? 1 : 0);
	}

	private static void Leave(GameObject gameObject){
		if (Main.room.canDissoluton) {
			API.Dissolution ();
		} else {
			Debug.Log ("不允许解散房间");
		}
	}
}
