﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BaseCompent:MonoBehaviour {


	public static void init(){
		
	}

	public void Update(){
//		Main.Update();
	}
	
	public static T GetGameObjectByName<T>(GameObject gameObject,string name){
		long begin = DateTime.Now.Ticks;
		GameObject obj = findChild (gameObject, name);
		if (obj != null) {
//			Debug.Log ("寻找控件:" + name + " 耗时:" + (DateTime.Now.Ticks - begin));
			return obj.GetComponent <T> ();
		}
		return default(T);
	}

	private static GameObject findChild(GameObject parent,string name){
		for (int i = 0; i < parent.transform.childCount; i++) {
			GameObject obj = parent.transform.GetChild(i).gameObject;
			if (obj.name.Equals (name)) {
				return obj;
			}else if (obj.transform.childCount > 0) {
				GameObject child = findChild (obj, name);
				if (child != null) {
					return child;
				}
			}
		}
		return null;
	}

}
