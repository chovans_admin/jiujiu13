﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;


//选手组件
public class PlayerCompent : BaseCompent {

	//选手信息,选手牌展示
	public static void PlayerInfo(GameObject widget,User currUser){

		UIButton button = GetGameObjectByName<UIButton> (widget, "Ready Button");
		if (button != null) {
			//添加事件
			UIEventListener.Get (button.gameObject).Clear();
			UIEventListener.Get (button.gameObject).onClick += ChangeReadyStatus;	

			//是否隐藏
			if (Main.user.status > USER_STATUS.CAN_ACTION.GetHashCode () 
				|| (Main.room.maxRound != Main.room.curRound && Main.room.curRound != 0)
				|| Main.room.players.Count != Main.room.playerNum) {
				button.gameObject.SetActive (false);
			} else {
				button.gameObject.SetActive (true);
			}
			//显示的sprite
			if (Main.user.status == USER_STATUS.READY.GetHashCode ()) {
				button.GetComponent<UIButton>().normalSprite = "Cancel";
			} else if (Main.user.status < USER_STATUS.READY.GetHashCode ()) {
				button.GetComponent<UIButton>().normalSprite = "Ready";
			}
		}

		UISprite inviteButton = GetGameObjectByName<UISprite> (widget, "Invite Button");
		if (inviteButton != null) {
			//添加事件
			UIEventListener.Get (inviteButton.gameObject).Clear();
			UIEventListener.Get (inviteButton.gameObject).onClick += InviteOther;	

			//是否隐藏
			if (Main.room.players.Count == Main.room.playerNum 
				|| Main.room.status > ROOM_STATUS.READY.GetHashCode()
				|| Main.room.curRound < Main.room.maxRound) {
				inviteButton.gameObject.SetActive (false);
			} else {
				inviteButton.gameObject.SetActive (true);
			}
		}

		//匹配名字，分别赋值
		UILabel nameLabel = GetGameObjectByName<UILabel>(widget,"Name");
		if (nameLabel != null) {
			nameLabel.text = currUser.username;
		}

		//是否房主
		UISprite iconMaster = GetGameObjectByName<UISprite>(widget,"Master");
		if (iconMaster != null && Main.room.owner != null && Main.room.owner.id == currUser.id) {
			iconMaster.gameObject.SetActive (true);
		} else if(iconMaster != null){
			iconMaster.gameObject.SetActive (false);
		}

		//头像
		UITexture avatar = GetGameObjectByName<UITexture>(widget,"Avatar");
		if (avatar != null && avatar.name == "Avatar") {
			try{
				avatar.name = "AvatarHasImg";
				Main.ImageDownload.SetAsyncImage(currUser.headImgUrl, avatar);	
			}catch(Exception e){
				Debug.Log ("加载用户头像失败:"+e.Message);
			}

		}

		//状态
		UISprite status = GetGameObjectByName<UISprite>(widget,"Status");
		if (status != null) {
			status.gameObject.SetActive (true);
			if (currUser.isOnline == false) {
				status.spriteName = "Icon Reconnet";
			} else if (currUser.status == USER_STATUS.READY.GetHashCode()) {
				status.spriteName = "Icon Ready";
			} else if (currUser.status == USER_STATUS.BEFORE_SUBMIT.GetHashCode()) {
				status.spriteName = "Icon Starting";
			} else if (currUser.status == USER_STATUS.AFTER_SUBMIT.GetHashCode()) {
				status.spriteName = "Icon Complate";
			} else {
				//不隐藏，其他if好操作
				status.spriteName = "null";
			}
		}

		//积分
		UILabel score = GetGameObjectByName<UILabel>(widget,"Score");
		if (score != null) {
			score.text = currUser.curTotal.ToString();
		}
	}

	public static void Reset(GameObject widget){
		//匹配名字，分别赋值
		UILabel nameLabel = GetGameObjectByName<UILabel>(widget,"Name");
		if (nameLabel != null) {
			nameLabel.text = "等待加入";
		}
	}

	//改变房间状态
	private static  void ChangeReadyStatus(GameObject gameObject){
		if (Main.user.status == USER_STATUS.READY.GetHashCode()) {
			API.ChangeRoomStatus (false);
		} else {
			API.ChangeRoomStatus (true);
		}
	}

	private static void InviteOther(GameObject gameObject){

		String info = Main.room.id + "|" + ScoreCompent.GetInfo () + "," + Main.room.payMethod
		              + "," + Main.room.maxRound + "局," + Main.room.playerNum+"人";

		Debug.Log (info);
		#if UNITY_ANDROID
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		jo.Call("invite",info);
		#endif

		#if UNITY_IOS
		SDKTool.invite (info);
		#endif

	}

}
