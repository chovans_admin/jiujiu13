﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//分数组件
public class ScoreCompent : BaseCompent {

	private static GameObject scoreSprite;

	//传入分数父级，自动填入参数
	public static void ScortInit(GameObject scoreSprite){
		
		ScoreCompent.scoreSprite = scoreSprite ;
		
		UILabel titleLabel = scoreSprite.transform.GetChild (0).GetComponent<UILabel> ();
		titleLabel.text = Main.room.payMethod + "/"+Main.room.playerNum.ToString()+"人";

		UILabel numberLabel = scoreSprite.transform.GetChild (2).GetComponent<UILabel> ();
		numberLabel.text = "房间号："+Main.room.id;
	}

	public static void Refresh(){
		if (scoreSprite == null) {
			return;
		}
		UILabel roundLabel = scoreSprite.transform.GetChild (1).GetComponent<UILabel> ();
		roundLabel.text = GetInfo() + "/第" + (Main.room.maxRound-Main.room.curRound) + "/" +Main.room.maxRound.ToString() + "局";
	}

	public static string GetInfo(){

		if (Main.room.extendColor1 != null && Main.room.extendColor1 != "") {
			return "加一色";
		} else if ((Main.room.extendColor1 != null && Main.room.extendColor1 != "") &&
		           (Main.room.extendColor2 != null && Main.room.extendColor2 != "")) {
			return "加两色";
		} else if (Main.room.withJiasanzhang == 3) {
			return "加三张";
		} else if (Main.room.withJiasanzhang == 4) {
			return "加四张";
		}
		return "普通模式";
	}

}
