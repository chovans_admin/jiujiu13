﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LicensingCompent : BaseCompent {

	private static List<UISprite> hasPick = new List<UISprite> ();			//提交的牌
	private static Dictionary<string,List<UISprite>> pockerMap = new Dictionary<string,List<UISprite>> ();
	private static Dictionary<string,List<string[]>> paixingCombo = new Dictionary<string, List<string[]>> ();
	private static List<UISprite> pickPokers = new List<UISprite> ();	//选中的牌
	private static UIPanel recommendPorkPanel = null;
	private static UIPanel pickPanel = null;		//选牌面板
	private static List<UISprite> pickPokerList = new List<UISprite> ();	//选牌面板里的牌0-12
	private static UIButton specialButton = null;
	private static UIAtlas atlas;
	private static TweenPosition originPosition = null;
	public static JSONObject SpecialResInfo = null;
	private static UISprite clockSprite = null;
	private static UILabel clockTime = null;

	void OnDetroy(){
		foreach (var u in Main.room.players) {
			Reset (u);
		}
	}

	//重置面板
	public static void Reset(User currUser){
		SpecialResInfo = null;

		Debug.Log("重置发牌面板"+currUser.username);

		if (currUser.id == Main.user.id) {
			foreach (var p in pickPokerList) {
				p.gameObject.SetActive (false);
			}

			foreach (var p in pickPokers) {
				Destroy (p);
			}
			pickPokers.Clear ();
			hasPick.Clear ();

			if (recommendPorkPanel != null) {
				recommendPorkPanel.gameObject.SetActive (false);
				pickPanel.gameObject.SetActive (false);
				paixingCombo = new Dictionary<string, List<string[]>> ();
				pickPokers = new List<UISprite> ();	//选中的牌
		//				minePokers = new List<UISprite> ();	//发给我的牌
			}
			//复位牌
			if(originPosition != null)
				originPosition.ResetToBeginning ();
		}

		List<UISprite> currList = new List<UISprite>();
		pockerMap.TryGetValue (currUser.id,out currList);

		//如果不在发牌状态,清空面板
		if(currList != null){
			foreach (var p in currList) {
				NGUITools.Destroy (p);
				Destroy (p);
			}
			currList.Clear ();	
		}

	}

	public static void HandlerTeshu(string result){
		//是否显示特殊牌型按钮
		SpecialResInfo = new JSONObject(result);
		specialButton.gameObject.SetActive( SpecialResInfo["type"].str != "");
	}

	//初始化方法
	public static void LicensingInit(UISprite parent,User currUser,UIAtlas atlas,UIPanel recommendPorkPanel,UIPanel pickPanel){

		Debug.Log ("初始化牌组");

		if (LicensingCompent.atlas == null) {
			LicensingCompent.atlas = atlas;
		}


		if (specialButton == null) {
			specialButton = GetGameObjectByName<UIButton>(pickPanel.gameObject,"Special");
			UIEventListener.Get (specialButton.gameObject).onClick += SubmiSpecial;
		}

		//检测是否含五同的房间
		if((Main.room.extendColor1 == null || Main.room.extendColor1 == "" || Main.room.withJiasanzhang > 0)
			&& recommendPorkPanel.transform.childCount == 9){
			NGUITools.Destroy (recommendPorkPanel.transform.GetChild (recommendPorkPanel.transform.childCount - 1).gameObject);
		}

		if(LicensingCompent.recommendPorkPanel == null && currUser.id == Main.user.id)
			LicensingCompent.recommendPorkPanel = recommendPorkPanel;
		if (LicensingCompent.pickPanel == null  && currUser.id == Main.user.id)
			LicensingCompent.pickPanel = pickPanel;

//		if (panelMap.ContainsKey (currUser.id)) {
//			panelMap.Add (currUser.id,parent);
//		}
	}


	//销毁牌
	public static void DestroyHiddenPokers(User user){
		GameObject parent = user.pokerContainer.gameObject;
		int count = parent.transform.childCount;
		for (int i = 0; i < count; i++) {
			UISprite poker = parent.transform.GetChild (0).gameObject.GetComponent<UISprite> ();
			NGUITools.Destroy (poker.transform);
			Destroy (poker.transform);
		}

		Debug.Log ("销毁后，剩余：" + parent.transform.childCount);
	}
		

	public static void InitHiddenPokers(User user,bool animation = true){

		UISprite parent = user.pokerContainer;
		parent.gameObject.SetActive(true);

		float totalWidth = parent.width;
		float offset = totalWidth / Main.user.cardList.Count;

		for (int i = 0; i < Main.user.cardList.Count; i++) {
			//放置位置，并隐藏
			UISprite sprite = NGUITools.AddSprite(parent.gameObject,LicensingCompent.atlas,"Card");

			sprite.gameObject.SetActive (true);
			sprite.width = (int)offset * 2;
			sprite.height = (int)(sprite.width * 1.4f);

			//位置
			float positionX = offset * i;
			sprite.transform.localPosition = new Vector3( positionX - (parent.width /2),0,-10);

			if (animation) {
				sprite.alpha = 0;
				sprite.transform.localRotation = new Quaternion (0, 90, 0, 1);

				//延时动画
				parent.StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
					{
						TweenAlpha.Begin(sprite.gameObject,0.1f,1);
						TweenRotation.Begin(sprite.gameObject,0.2f,new Quaternion (0, 0, 0, 1));
					}, (0.05f * i)));	

				EffectUtil.FaPai ();
			}
		}
	}

	public static void ShowHiddenPokers(User user){
		user.pokerContainer.gameObject.SetActive (true);
	}

	public static void HideHiddenPokers(User user){
		user.pokerContainer.gameObject.SetActive (false);
	}

	public static void SwitchHiddenPokers(User user){

		// BEFORE_SUBMIT , AFTER_SUBMIT
		// !1 long || 355
		//出牌后修改位置
		if (user.status == USER_STATUS.AFTER_SUBMIT.GetHashCode ()) {
			for (int i = 0; i < 13; i++) {
				UISprite sprite = user.pokerContainer.transform.GetChild (i).transform.gameObject.GetComponent<UISprite> ();
				//和结果面板大小一样
				sprite.width = 300;
				sprite.height = 400;
				float y = (i > 7 ? -sprite.height / 2 : i > 2 ? 0 : sprite.height / 2) - (sprite.height / 1.3f);
				double x = i > 7 ? ((i - 7)*sprite.width - (sprite.width * 3)) : 
					i > 2 ? ((i - 2)*sprite.width - (sprite.width * 3)) : ((i - 1)*sprite.width);
				x = sprite.transform.localScale.x * x / 2f;
				y = sprite.transform.localScale.y * y / 1.5f;
				TweenPosition.Begin(sprite.gameObject,0.3f,new Vector3(float.Parse(x.ToString()),y,-10));
			}
		}
	}

	public static void SwitchMyPokers(){
		SwitchHiddenPokers (Main.room.players[0]);
	}

	public static void InitMyPokers(bool animation = true){

		User user = Main.room.players [0];
		InitHiddenPokers (user,animation);

	}

	public static void AnimationMyPokers(){
		User user = Main.room.players [0];
		//翻牌
		user.pokerContainer.StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
			{
				CheckPaixing();
				OpenCards(user.pokerContainer);
				IsShowPaixing (true);
			}, 1f));
	}

	public static void InitGamingPanel(UIAtlas atlas,UIPanel recommendPorkPanel,UIPanel pickPanel){
		if (LicensingCompent.atlas == null) {
			LicensingCompent.atlas = atlas;
		}

		if (specialButton == null) {
			specialButton = GetGameObjectByName<UIButton>(pickPanel.gameObject,"Special");
			UIEventListener.Get (specialButton.gameObject).onClick += SubmiSpecial;
		}

		if(LicensingCompent.recommendPorkPanel == null)
			LicensingCompent.recommendPorkPanel = recommendPorkPanel;
		
		if (LicensingCompent.pickPanel == null)
			LicensingCompent.pickPanel = pickPanel;

		//检测是否含五同的房间(有花色，并且不为加3、4张)
		if((Main.room.extendColor1 == null || Main.room.extendColor1 == "" ) 
			&& Main.room.withJiasanzhang == 0
			&& recommendPorkPanel.transform.childCount == 9){
			NGUITools.Destroy (recommendPorkPanel.transform.GetChild (recommendPorkPanel.transform.childCount - 1).gameObject);
		}


	}


	public static void ShowMyPokers(){
		ShowHiddenPokers (Main.room.players [0]);
	}

	public static void HideMyPokers(User user){
		HideHiddenPokers (Main.room.players [0]);
	}

	public static void DestroyMyPokers(){
		DestroyHiddenPokers (Main.room.players [0]);
		//隐藏
		pickPanel.gameObject.SetActive(false);
		IsShowPaixing (false);
		//清空已选牌
		pickPokers.Clear ();
		hasPick.Clear ();
		foreach (var p in pickPokerList) {
			p.gameObject.SetActive (false);
		}
	}

	//检测牌型
	private static void CheckPaixing(){
		//清空牌型
		paixingCombo.Clear();

		var cl = new CardList();

		//去除已选的牌检测
		List<string> surplusCards =  new List<string>(Main.user.cardList);

		foreach (var i in hasPick) {
			string p = Utils.getServerPorker (i.spriteName);
			surplusCards.Remove (p);
		}

		paixingCombo = Utils.PaixingCombo (cl,surplusCards.ToArray());

		//检测哪些牌型亮起
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"DuiZi"),"duizi");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"LiangDui"),"liangdui");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"SanTiao"),"santiao");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"ShunZi"),"shunzi");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"HuLu"),"hulu");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"TongHua"),"tonghua");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"ZhaDan"),"zhadan");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"TongHuaShun"),"tonghuashun");
		LightPaixing(GetGameObjectByName<UISprite>(recommendPorkPanel.gameObject,"WuTong"),"wutong");

		//显示的牌收拢
		UISprite parent = Main.room.players[0].pokerContainer;
		List<UISprite> pokers = new List<UISprite> ();
		for (int i = 0; i < parent.transform.childCount; i++) {
			if (parent.transform.GetChild (i).gameObject.activeInHierarchy)
				pokers.Add (parent.transform.GetChild (i).gameObject.GetComponent<UISprite>());
		}
		if (pokers.Count > 0) {
			float totalWidth = pokers.Count * pokers [0].width / 2;
			float offset = totalWidth / pokers.Count;
			for (int i = 0; i < pokers.Count; i++) {
				UISprite sprite = pokers [i];
				float positionX = offset * i;
				TweenPosition.Begin (pokers [i].gameObject, 0.1f
					, new Vector3( positionX - (totalWidth / 2),0,-10));
			}
		}
	}

	//点亮牌型
	private static void LightPaixing(UISprite sprite,string key){
		if (sprite == null) {
			Debug.Log ("查找牌型：" + key+"失败");
			return;
		}
			
		IEnumerable<string[]> items = new IEnumerable<string[]> ();
		if (paixingCombo.ContainsKey (key)) {
			paixingCombo.TryGetValue (key, out items);
		}
		sprite.spriteName = key + "_0" + (items.Any() ? "1":"2");

		if (sprite.GetComponent<BoxCollider> () == null) {
			BoxCollider boxcollider = sprite.gameObject.AddComponent<BoxCollider> ();
			boxcollider.center = new Vector3 (sprite.width/2, -sprite.height/2);
			boxcollider.size = new Vector3 (sprite.width, sprite.height);
			UIEventListener.Get (sprite.gameObject).onClick += AutoPrompt;	
		}
	}

	//翻牌
	public static void OpenCards(UISprite parent){
		
		User user = Main.room.players [0];
		for (int i = 0; i < Main.user.cardList.Count; i++) {
			user.pokerContainer.transform.GetChild(i).transform.gameObject.GetComponent<UISprite>().spriteName = Utils.getLocalPorker(Main.user.cardList [i]);
		}

		parent.StartCoroutine(DelayToInvoke.DelayToInvokeDo(() =>
			{
				//整体位置下移
				originPosition = TweenPosition.Begin(parent.gameObject,1,
					new Vector3(0,-980,-10));



			}, 0.5f));

		//动画完成后添加事件
		parent.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			for (int i = 0; i < Main.user.cardList.Count; i++) {
				UISprite sprite = user.pokerContainer.transform.GetChild(i).transform.gameObject.GetComponent<UISprite>();
				BoxCollider boxcollider = sprite.gameObject.AddComponent<BoxCollider> ();
				boxcollider.size = new Vector3 (sprite.width, sprite.height);
				UIEventListener.Get (sprite.gameObject).onClick += IsPokerShow;	
				UIEventListener.Get(sprite.gameObject).onDragOver += IsPokerShow2;
			}

			//检测是否完成摆牌
			CheckIsPickComplate ();

			UISprite line1 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Line1");
			UISprite line2 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Line2");
			UISprite line3 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Line3");

			UIEventListener.Get (line1.gameObject).Clear();
			UIEventListener.Get (line2.gameObject).Clear();
			UIEventListener.Get (line3.gameObject).Clear();
			UIEventListener.Get (line1.gameObject).onClick += PickUpLine1;
			UIEventListener.Get (line2.gameObject).onClick += PickUpLine2;
			UIEventListener.Get (line3.gameObject).onClick += PickUpLine3;	

			//清理事件
			UISprite clearLine1 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Clear Line1");
			UISprite clearLine2 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Clear Line2");
			UISprite clearLine3 = GetGameObjectByName<UISprite>(pickPanel.gameObject,"Clear Line3");

			UIEventListener.Get (clearLine1.gameObject).Clear();
			UIEventListener.Get (clearLine2.gameObject).Clear();
			UIEventListener.Get (clearLine3.gameObject).Clear();
			UIEventListener.Get (clearLine1.gameObject).onClick += ClearPickerLine1;
			UIEventListener.Get (clearLine2.gameObject).onClick += ClearPickerLine2;
			UIEventListener.Get (clearLine3.gameObject).onClick += ClearPickerLine3;

			UIButton surelButton = GetGameObjectByName<UIButton> (pickPanel.gameObject, "Sure Button");
			UIEventListener.Get (surelButton.gameObject).Clear();
			UIEventListener.Get (surelButton.gameObject).onClick += Submit;

			UIButton cancelButton = GetGameObjectByName<UIButton> (pickPanel.gameObject, "Cancel Button");
			UIEventListener.Get (cancelButton.gameObject).Clear();
			UIEventListener.Get (cancelButton.gameObject).onClick += ClearPickerLineAll;
		}, 0.5f * 13 / 4));

		//延时音效
		parent.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			EffectUtil.QingChuPai();
		}, 2f));

		//倒计时
		if (clockSprite == null)
			clockSprite = GetGameObjectByName<UISprite> (pickPanel.gameObject, "Clock");
		if (clockTime == null)
			clockTime = GetGameObjectByName<UILabel> (pickPanel.gameObject, "Time");
		clockSprite.gameObject.SetActive (true);
		clockTime.text = "300";

		//牌型提示
		CheckPaixing();

//		Bounds bounds = NGUIMath.CalculateRelativeWidgetBounds(recommendPorkPanel.gameObject.transform);

//		float width = recommendPorkPanel.width / (recommendPorkPanel.transform.childCount + 1) * 0.92f;
//		float offset = recommendPorkPanel.width / (recommendPorkPanel.transform.childCount);
//		for (int i = 0; i < recommendPorkPanel.transform.childCount; i++) {
//			GameObject obj = recommendPorkPanel.transform.GetChild (i).gameObject;
//			Vector3 curr = obj.transform.position;
//			obj.GetComponent<UISprite>().width = (int)width;
//			obj.transform.position = new Vector3(( (float)(i * offset) - (recommendPorkPanel.width / 2)) / 360,curr.y,curr.z);
//		}
	}
		
	public static float timer = 1.0f;

	public static void Update(){
		if (recommendPorkPanel == null || !recommendPorkPanel.gameObject.activeInHierarchy)
			return;
		float width = recommendPorkPanel.width / (recommendPorkPanel.transform.childCount);
		float offset = recommendPorkPanel.width / (recommendPorkPanel.transform.childCount);
		for (int i = 0; i < recommendPorkPanel.transform.childCount; i++) {
			GameObject obj = recommendPorkPanel.transform.GetChild (i).gameObject;
			Vector3 curr = obj.transform.position;
			obj.GetComponent<UISprite>().width = (int)width;
			obj.transform.position = new Vector3(( (float)(i * offset) - (recommendPorkPanel.width / 2)) / 360,curr.y,curr.z);
		}

		timer -= Time.deltaTime;
		if (timer <= 0) {
			int curr = int.Parse (clockTime.text);
			if (curr < 10) {
				clockTime.color = Color.red;
			} else {
				clockTime.color = Color.white;
			}
			clockTime.text = (curr-1).ToString();
			timer = 1.0f;
		}
	}

		
	private static void PickUpLine1(GameObject gameObject){
		PickUpLine (1);
	}
	private static void PickUpLine2(GameObject gameObject){
		PickUpLine (2);
	}
	private static void PickUpLine3(GameObject gameObject){
		PickUpLine (3);
	}

	//点击放到三道里
	private static void PickUpLine(int line){
		//将摆牌面板上的13张牌加入到临时变量，易于控制
		if(pickPokerList.Count == 0 || pickPokerList[0] == null){
			pickPokerList.Clear ();
			UISprite pokerSprite = GetGameObjectByName<UISprite> (pickPanel.gameObject, "Pokers");
			for (int i = 0; i < pokerSprite.transform.childCount; i++) {
				UISprite s = pokerSprite.transform.GetChild (i).gameObject.GetComponent<UISprite> ();
				if(s != null)
					pickPokerList.Add (s);
			}
		}

		if (line == 0) {
			foreach (var p in pickPokerList) {
				if (!p.gameObject.activeInHierarchy) {
					p.spriteName = pickPokers [0].spriteName;
					hasPick.Add (pickPokers [0]);
					pickPokers [0].gameObject.SetActive (false);
					pickPokers.RemoveAt (0);
					p.gameObject.SetActive (true);
				}
			}

		}

		int start = line == 1 ? 0 : line == 2 ? 3 : line == 3 ? 8 : 0;
		int end = line == 1 ? 3 : line == 2 ? 8 : line == 3 ? 13 : 0;
		for (int i = start; i < end; i++) {
			if (!pickPokerList [i].gameObject.activeInHierarchy && pickPokers.Count > 0) {
				pickPokerList [i].gameObject.SetActive (true);
				pickPokerList [i].spriteName = pickPokers [0].spriteName;	
				pickPokers [0].gameObject.SetActive (false);
				hasPick.Add (pickPokers [0]);
				pickPokers.RemoveAt (0);
			}
		}
		pickPokers.Clear ();

		//检测是否剩余
		if(Main.user.cardList.Count == 13 && (Main.user.cardList.Count - hasPick.Count == 3 || Main.user.cardList.Count - hasPick.Count == 5) && hasPick.Count != 13){
//		if(hasPick.Count == 8 || hasPick.Count == 10){

			User user = Main.room.players [0];
			for (int i = 0; i < Main.user.cardList.Count; i++) {
				UISprite p = user.pokerContainer.transform.GetChild (i).transform.gameObject.GetComponent<UISprite> ();
				if (p.gameObject.activeInHierarchy) {
					pickPokers.Add (p);
				}
			}
			PickUpLine (0);
		}
			
		//检测是否完成摆牌
		CheckIsPickComplate ();

		//重新检测牌型
		CheckPaixing ();

	}

	private static int CalFreeGrid(int line){
		int num = 0;
		int start = line == 1 ? 0 : line == 2 ? 3 : line == 3 ? 8 : 0;
		int end = line == 1 ? 3 : line == 2 ? 8 : line == 3 ? 13 : 0;
		for (int i = start; i < end; i++) {
			if (!pickPokerList [i].gameObject.activeInHierarchy)
				num++;
		}
		return num;
	}

	//是否选中待选牌
	private static void IsPokerShow(GameObject gameObject){
		UISprite card = gameObject.GetComponent<UISprite>();
		float distance = 100;
		if (!pickPokers.Contains (card)) {
			pickPokers.Add (card);
		} else {
			distance = 0;
			pickPokers.Remove (card);
		} 
		TweenPosition.Begin (card.gameObject, 0.1f, 
			new Vector3 (card.transform.position.x * 360, (card.transform.position.y + distance), -10));

		EffectUtil.DianPai ();
	}

	//滑动选牌
	private static void IsPokerShow2(GameObject gameObject){
		UISprite card = gameObject.GetComponent<UISprite>();

		float distance = 100;
		if (!pickPokers.Contains (card)) {
			pickPokers.Add (card);
		} else {
			return;
		}
		TweenPosition.Begin (card.gameObject, 0.1f, 
			new Vector3 (card.transform.position.x * 360, (card.transform.position.y + distance), -10));

		EffectUtil.DianPai ();
	}


	//是否显示牌型提示
	private static void IsShowPaixing(bool show){
		recommendPorkPanel.gameObject.SetActive (show);
		for (int i = 0; i < recommendPorkPanel.transform.childCount; i++) {
			GameObject obj = recommendPorkPanel.transform.GetChild (i).gameObject;
			obj.gameObject.SetActive (show);
		}
	}


	private static void ClearPickerLineAll(GameObject gameobject){
		ClearPickerPanel (0);
	}
	private static void ClearPickerLine1(GameObject gameobject){
		ClearPickerPanel (1);
	}
	private static void ClearPickerLine2(GameObject gameobject){
		ClearPickerPanel (2);
	}
	private static void ClearPickerLine3(GameObject gameobject){
		ClearPickerPanel (3);
	}

	private static void ClearPickerPanel(int line){
		int start = (line == 0 || line == 1?0: line == 2? 3 : line == 3 ? 8:0);
		int end = (line == 0?13: line == 1 ? 3 : line == 2? 8 : line == 3 ? 13:13);
		for (int i = start; i < end; i++) {
			UISprite sprite = pickPokerList [i];
			//重新显示牌
			User user = Main.room.players [0];
			for (int j = 0; j < Main.user.cardList.Count; j++) {
				UISprite p = user.pokerContainer.transform.GetChild (j).transform.gameObject.GetComponent<UISprite> ();
				if (p.spriteName.Equals (sprite.spriteName)) {
					//从已选择中去除
					hasPick.Remove (p);
					TweenPosition.Begin (p.gameObject, 0.1f, 
						new Vector3 (p.transform.position.x * 360, p.transform.position.y, -10));

					p.gameObject.SetActive (true);
//					IsPickPokerStyle (false, p);
				}
			}

//			foreach(var p in minePokers){
//				if (p.spriteName.Equals (sprite.spriteName)) {
//					//从已选择中去除
//					hasPick.Remove (p);
//					TweenPosition.Begin (p.gameObject, 0.1f, 
//						new Vector3 (p.transform.position.x * 360, p.transform.position.y, -10));
//
////					p.gameObject.SetActive (true);
//					IsPickPokerStyle (false, p);
//				}
//			}

			//设置面板
			pickPokerList[i].gameObject.SetActive(false);
			pickPokerList[i].spriteName = "null";
		}

		//检测是否完成摆牌
		CheckIsPickComplate ();
		//重新检测牌型
		CheckPaixing ();
	}

	//检测是否完成摆牌
	private static void CheckIsPickComplate(){
		pickPanel.gameObject.SetActive (true);
		UIButton sureButton = GetGameObjectByName<UIButton> (pickPanel.gameObject, "Sure Button");
		UIButton cancelButton = GetGameObjectByName<UIButton> (pickPanel.gameObject, "Cancel Button");
		if (hasPick.Count == 13) {
			sureButton.gameObject.SetActive (true);
			cancelButton.gameObject.SetActive (true);

			//检测是否倒水
			checkIsFail ();

		} else {
			sureButton.gameObject.SetActive (false);
			cancelButton.gameObject.SetActive (false);
			UILabel alert = GetGameObjectByName<UILabel> (pickPanel.gameObject, "Alert");
			alert.gameObject.SetActive (false);
		}

	}

	//检测是否倒水
	private static void checkIsFail(){
		List<Card> pokers = new List<Card> ();
		foreach (var p in pickPokerList) {
			pokers.Add (Card.GetIns(Utils.getServerPorker (p.spriteName)));
		}

		if (pokers.Count < 13) {
			setSubmitButtomDisabled(false);// 解除禁用出牌按钮
			return;
		}

		CardList top = new CardList(pokers.GetRange (0, 3));
		CardList mid = new CardList(pokers.GetRange (3, 5));
		CardList btm = new CardList(pokers.GetRange (8, 5));

		//初始化重置
		setSubmitButtomDisabled(true);// 禁用出牌按钮
		setSubmitErrorMessage("");// 清空错误消息

		//中尾倒水情况
		if(!Utils.MidVsBtm(mid,btm)){
			setSubmitErrorMessage("中尾倒水");
			return;
		};
		//中尾倒水情况
		if(!Utils.TopVsMid(top,mid)){
			setSubmitErrorMessage("头中倒水");
			return;
		};

		setSubmitButtomDisabled(false);// 解除禁用出牌按钮
	}
	// 禁用出牌按钮 与否
	private static void setSubmitButtomDisabled(bool is_disabled){
		UIButton sureButton = GetGameObjectByName<UIButton> (pickPanel.gameObject, "Sure Button");
		UILabel alert = GetGameObjectByName<UILabel> (pickPanel.gameObject, "Alert");
		if (is_disabled) {
//			sureButton.normalSprite = "OutCard2";
			sureButton.isEnabled = false;
			alert.gameObject.SetActive (true);
		} else {
//			sureButton.normalSprite = "OutCard1";
			sureButton.isEnabled = true;
			alert.gameObject.SetActive (false);
		}
	}
	// 设置倒水信息
	private static void setSubmitErrorMessage(string err_msg){
		UILabel alert = GetGameObjectByName<UILabel> (pickPanel.gameObject, "Alert");
		alert.text = err_msg;
	}

	private Dictionary<IEnumerable<string[]>,IEnumerator<string[]>> combosEnumeratorMap = new Dictionary<IEnumerable<string[]>,IEnumerator<string[]>>();
	//牌型提示
	private static void AutoPrompt(GameObject gameObject){
		UISprite sprite = gameObject.GetComponent<UISprite> ();
		string key = sprite.spriteName.Split ('_') [0];
		IEnumerable<string[]> combos = new IEnumerable<string[]> ();
		paixingCombo.TryGetValue (key,out combos);
		if (!combos.Any()) {
			return;
		}
		//清空所有牌的选中状态
		User user = Main.room.players [0];
		for (int i = 0; i < Main.user.cardList.Count; i++) {
			UISprite p = user.pokerContainer.transform.GetChild (i).transform.gameObject.GetComponent<UISprite> ();
			if (pickPokers.Contains (p)) {
				IsPokerShow (p.gameObject);
			}
		}
//		foreach(var p in minePokers){
//			if (pickPokers.Contains (p)) {
//				IsPokerShow (p.gameObject);
//			}
//		}

		IEnumerator<string[]> combosEnumerator = combosEnumeratorMap.ContainsKey(combos) ? combosEnumeratorMap[combos] : (combosEnumeratorMap[combos] = combos.GetEnumerator());

		if(!combosEnumerator.MoveNext()){
			// 迭代完成，重新初始化一个迭代器
			combosEnumeratorMap[combos] = combos.GetEnumerator();
		}

		//添加选中状态
		string[] combo = combosEnumerator.Current;
		foreach (var c in combo) {

			for (int i = 0; i < Main.user.cardList.Count; i++) {
				UISprite p = user.pokerContainer.transform.GetChild (i).transform.gameObject.GetComponent<UISprite> ();
				if (Utils.getServerPorker (p.spriteName).Equals (c)) {
					//如果匹配,选中状态
					IsPokerShow(p.gameObject);
					break;
				}
			}

//			foreach (var p in minePokers) {
//				if (Utils.getServerPorker (p.spriteName).Equals (c)) {
//					//如果匹配,选中状态
//					IsPokerShow(p.gameObject);
//					break;
//				}
//			}
		}
	}

	private static void Submit(GameObject gameObject){
//		pickPanel.gameObject.SetActive (false);
		List<string> pokers = new List<string> ();
		foreach (var p in pickPokerList) {
			pokers.Add (Utils.getServerPorker (p.spriteName));
		}
		API.Submit (pokers.GetRange (0, 3).ToArray (), pokers.GetRange (3, 5).ToArray (), pokers.GetRange (8, 5).ToArray ());
	}

	//特殊牌型提示
	private static void SubmiSpecial(GameObject gameObject){
		TeShuCompent.Show ();
//		API.SubmitSpecial ();
	}
}
