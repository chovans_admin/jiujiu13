﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DissolutionCompent : BaseCompent {

	//计算时间
	private static int waitTime = 0;
	private static int botDuration = 300;
	private static GameObject rootPanel = null;
	private static UIPanel panel = null;

	//初始化
	public static void DissolutionInit(GameObject _rootPanel,UIPanel _panel){
		if (rootPanel != null || panel != null) {
			return;
		}
		rootPanel = _rootPanel;
		panel = _panel;

//		DissolutionCompent.InvokeRepeating ("calTime", 0, 1);

		//添加同意和拒绝事件
		UIEventListener.Get(GetGameObjectByName<UISprite>(panel.gameObject,"Sure Button").gameObject).onClick += Sure;
		UIEventListener.Get(GetGameObjectByName<UISprite>(panel.gameObject,"Cancel Button").gameObject).onClick += Cancel;
	}

	//检测房间是否解散成功
//	public static bool CheckDissolution(){
//		return Main.user.status == USER_STATUS.FREE.GetHashCode ();
//	}

	//开始解散房间
	public static void StartDisSolution(string jsonString){
		if (Main.room.players.Count == 1) {
			SceneManager.LoadScene ("Home");
		} else {
			ShowWait (false);
			panel.gameObject.SetActive (true);
			Debug.Log ("开始解散房间");
		}
	}

	//解散失败
	public static void DissolutionFail(){
		panel.gameObject.SetActive (false);
	}

	//重置时间
	public static void resetTime(){
		waitTime = 0;
	}

	//统计时间
	public static void calTime(){
		waitTime += 1;
//		if (waitTime > botDuration) {
//			Main.Instance.CloseSocket ();
//			panel.gameObject.SetActive (true);
//		}
	}

	//设置配置信息
	public static void Setting(string jsonString){
		JSONObject json = new JSONObject (jsonString);
		botDuration = int.Parse (json ["bot_duration"].ToString ()) / 1000;
	}

	//同意解散消息
	public static void Sure(GameObject gameObject){
		API.Dissolution ();
		ShowWait (true);
	}

	//拒绝解散
	public static void Cancel(GameObject gameObject){
		API.DisagreeDissolution ();
		ShowWait (true);
	}

	public static void ShowWait(bool show){
		GetGameObjectByName<UILabel> (panel.gameObject, "Wait").gameObject.SetActive (show);
		GetGameObjectByName<UISprite> (panel.gameObject, "Sure Button").gameObject.SetActive (!show);
		GetGameObjectByName<UISprite> (panel.gameObject, "Cancel Button").gameObject.SetActive (!show);
	}
}

