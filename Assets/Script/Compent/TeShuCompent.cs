﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeShuCompent : BaseCompent {

	private static UIPanel panel;
	private static bool isShow = false;
	private static Rule rule = new Rule();

	private static CardBox _ = new CardBox(true, "黑桃*", "红心*");

	static TeShuCompent(){
		_.Init();
	}

	public static void Reset(){
		isShow = false;
	}

	public static void CheckTeShu(){
		if(Main.user.cardList.Count == 13)
			API.CheckSpecial ();
	}

	public static void ShowMyTeShu(string result){
		JSONObject ts = new JSONObject (result);
		string type = "";
		if (ts != null) {
			type = ts ["type"].str;
		}
		if (type.Equals("KingLong")) {
			Alert ("zhizunqinglong");
		} else if (type.Equals("OneLong")) {
			Alert ("yitiaolong");
		} else if (type.Equals("SixPair")) {
			Alert ( "liuduiban");
		}else if (type.Equals("ThreeFlush")) {
			Alert ( "santonghua");
		}else if (type.Equals("AllSmall")) {
			Alert ( "quanxiao");
		}else if (type.Equals("AllBig")) {
			Alert ( "quanda");
		}else if (type.Equals("ThreeStraight")) {
			Alert ("sanshunzi");
		}
		//add
		else if (type.Equals("FourThree")) {
			Alert ("sitaosantiao");
		} else if (type.Equals("ThreeFour")) {
			Alert ( "sanfentianxia");
		}else if (type.Equals("TwoFiveAndOneThree")) {
			Alert ( "sanhuangwudi");
		}else if (type.Equals("TwelveRoyal")) {
			Alert ( "shierhuangzu");
		}else if (type.Equals("OneSix")) {
			Alert ("liuliudashun");
		}else if (type.Equals("FivePairAndOneThree")) {
			Alert ( "wuduisantiao");
		}else if (type.Equals("SameColor")) {
			Alert ( "couyise");
		}else if (type.Equals("ThreeStraightFlush")) {
			Alert ("santonghuashun");
		}
	}

	public static void TeShuCompentInit(UIPanel panel){
		if (TeShuCompent.panel == null)
			TeShuCompent.panel = panel;

		/*
		var cl = new CardList();
		AddCardsByTxt (cl, Main.user.cardList.ToArray());
		Is_SpecialResInfo res_info;

		if (rule.IsKingLong (cl, out res_info)) {
			Alert ("zhizunqinglong");
		} else if (rule.IsOneLong (cl,out  res_info)) {
			Alert ("yitiaolong");
		} else if (rule.IsSixPair (cl,out  res_info)) {
			Alert ( "liuduiban");
		}else if (rule.IsThreeFlush (cl,out  res_info)) {
			Alert ( "santonghua");
		}else if (rule.IsAllSmall (cl,out  res_info)) {
			Alert ( "quanxiao");
		}else if (rule.IsAllBig (cl,out  res_info)) {
			Alert ( "quanda");
		}else if (rule.IsThreeStraight (cl,out  res_info)) {
			Alert ("sanshunzi");
		}
		//add
		else if (rule.IsFourThree (cl,out  res_info)) {
			Alert ("sitaosantiao");
		} else if (rule.IsThreeFour (cl,out  res_info)) {
			Alert ( "sanfentianxia");
		}else if (rule.IsTwoFiveAndOneThree (cl,out  res_info)) {
			Alert ( "sanhuangwudi");
		}else if (rule.IsTwelveRoyal (cl,out  res_info)) {
			Alert ( "shierhuangzu");
		}else if (rule.IsOneSix (cl,out  res_info)) {
			Alert ("liuliudashun");
		}else if (rule.IsFivePairAndOneThree (cl,out  res_info)) {
			Alert ( "wuduisantiao");
		}else if (rule.IsSameColor (cl,out  res_info)) {
			Alert ( "couyise");
		}else if (rule.IsThreeStraightFlush (cl,out  res_info)) {
			Alert ("santonghuashun");
		}
*/
		//检测是否特殊牌型
//		if (rule.IsKingLong (cl, out res_info)) {
//			Long (panel,"zhizunqinglong");
//		} else if (rule.IsOneLong (cl,out  res_info)) {
//			Long (panel,"yitiaolong");
//		} else if (rule.IsSixPair (cl,out  res_info)) {
//			Yuan (panel, "liuduiban");
//		}else if (rule.IsThreeFlush (cl,out  res_info)) {
//			Yuan (panel, "santonghua");
//		}else if (rule.IsAllSmall (cl,out  res_info)) {
//			Yuan (panel, "quanxiao");
//		}else if (rule.IsAllBig (cl,out  res_info)) {
//			Yuan (panel, "quanda");
//		}
//		else if (rule.IsThreeStraight (cl,out  res_info)) {
//			Debug.Log ("三对子，没有图片，不显示");
////			Yuan (panel, "sanduizi");
//		}
	}

	public static void Show(){
		TeShuCompent.panel.gameObject.SetActive (true);
	}

	private static void Alert(string spriteName){
		TeShuCompent.panel.gameObject.SetActive (true);
		UISprite sureBtn = GetGameObjectByName<UISprite> (TeShuCompent.panel.gameObject, "Sure");
		UISprite cancel = GetGameObjectByName<UISprite> (TeShuCompent.panel.gameObject, "Cancel");
		UISprite paixing = GetGameObjectByName<UISprite> (TeShuCompent.panel.gameObject, "Paixing");

		paixing.spriteName = spriteName;
		UIEventListener.Get (sureBtn.gameObject).Clear ();
		UIEventListener.Get (sureBtn.gameObject).onClick += SendResult;
		UIEventListener.Get (cancel.gameObject).Clear ();
		UIEventListener.Get (cancel.gameObject).onClick += Hide;
	}


	private static void  SendResult(GameObject gameObject){
		API.SubmitSpecial ();
		panel.gameObject.SetActive (false);
	}
	private static void  Hide(GameObject gameObject){
		panel.gameObject.SetActive (false);
	}
		

	/// <summary>
	/// 	以下不用
	/// </summary>
	/// <param name="panel">Panel.</param>
	/// <param name="name">Name.</param>
	//至尊青龙，一条龙
	private static void Long(UIPanel panel,string name){
		
		UIWidget longWidget = GetGameObjectByName<UIWidget> (panel.gameObject, "Long");
		longWidget.gameObject.SetActive (true);

		Bounds bounds = NGUIMath.CalculateRelativeWidgetBounds(panel.gameObject.transform);


		float width = (bounds.max.x - bounds.min.x);
		float pokerWidth = width / 13;

		for (int i = 0; i < longWidget.transform.childCount; i++) {
			GameObject gameObject = longWidget.transform.GetChild (i).gameObject;
			float x = i * pokerWidth - (width / 2) + (pokerWidth / 2);
			TweenPosition.Begin (gameObject, 0.001f, new Vector3 (x,gameObject.transform.position.y,-10));
			CardTransform (panel,gameObject,i);
		}

		TweenPosition.Begin (longWidget.gameObject, 0.001f, new Vector3 (-width*1.5f,longWidget.gameObject.transform.position.y,-10));

		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			TweenPosition.Begin (longWidget.gameObject, 1f, new Vector3 (0,longWidget.gameObject.transform.position.y,-10));
		}, 1f));


		//显示图标
		UISprite result = GetGameObjectByName<UISprite> (panel.gameObject, "Sprite");
		result.gameObject.SetActive (true);
		result.alpha = 0;
		result.spriteName = name;

		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			TweenAlpha.Begin(result.gameObject,0.1f,1);
		}, 4f));

		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			EffectUtil.TeShuPaiXing(name);
		}, 4f));
		close (panel);
	}

	public static void Yuan(UIPanel panel,string name){
		
		panel.gameObject.SetActive (true);
		UIWidget longWidget = GetGameObjectByName<UIWidget> (panel.gameObject, "Yuan");
		longWidget.gameObject.SetActive (true);

		for (int i = 0; i < longWidget.transform.childCount; i++) {
			GameObject gameObject = longWidget.transform.GetChild (i).gameObject;
			gameObject.GetComponent<UISprite> ().spriteName = "Card";
			YuanTransfrom (panel, gameObject, i);
		}

		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			EffectUtil.TeShuPaiXing(name);
		}, 4f));

		//显示图标
		UISprite result = GetGameObjectByName<UISprite> (panel.gameObject, "Sprite");
		result.gameObject.SetActive (true);
		result.alpha = 0;
		result.spriteName = name;

		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			TweenAlpha.Begin(result.gameObject,0.1f,1);
		}, 4f));

		close (panel);
	}

	public static void YuanTransfrom(UIPanel panel,GameObject gameObject,int i){
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			gameObject.GetComponent<UISprite>().spriteName = Utils.getLocalPorker(Main.user.cardList[i]);
		}, 0.2f*i));
	}

	public static void CardTransform(UIPanel panel,GameObject gameObject,int i){
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			TweenRotation.Begin(gameObject,1f,new Quaternion (0, 90, 0, 1));
		}, 2f));
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			gameObject.GetComponent<UISprite>().spriteName = Utils.getLocalPorker(Main.user.cardList[i]);
			TweenRotation.Begin(gameObject,1f,new Quaternion (0, 0, 0, 1));
		}, 2.5f));
	}

	public static void AddCardsByTxt(CardList cl, string[] txts, bool is_clear = true){
		if (is_clear)
		{
			cl.Clear();
		}
		foreach (var txt in txts)
		{
			Card card = Card.GetIns (txt);
			cl.Add(card);
		}
	}

	public static void close (UIPanel panel){
		panel.StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			panel.gameObject.SetActive(false);
		}, 6f));
	}
}
