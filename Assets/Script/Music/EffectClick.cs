﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectClick : MonoBehaviour {

	public UISprite effect;

	// Use this for initialization
	void Start () {
		Toggle ();
	}

	void OnMouseDown(){
		PlayerPrefs.SetInt (Main.EFFECT,PlayerPrefs.GetInt (Main.EFFECT) == 0 ? 1 : 0);
		Toggle ();
	}

	public void Toggle(){
		if (PlayerPrefs.GetInt (Main.EFFECT) == null) {
			PlayerPrefs.SetInt (Main.EFFECT,1);
		}
		if (PlayerPrefs.GetInt (Main.EFFECT) == 0) {
			effect.spriteName = "sound_close";
		} else {
			effect.spriteName = "sound_open";
		}
	}
}
