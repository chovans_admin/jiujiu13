﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicClick : MonoBehaviour {

	public UISprite music;

	// Use this for initialization
	void Start () {
		Toggle ();
	}

	void OnMouseDown(){
		PlayerPrefs.SetInt (Main.MUSIC,PlayerPrefs.GetInt (Main.MUSIC) == 0 ? 1 : 0);
		Toggle ();
	}

	public void Toggle(){
		if (PlayerPrefs.GetInt (Main.MUSIC) == null) {
			PlayerPrefs.SetInt (Main.MUSIC,1);
		}
		if (PlayerPrefs.GetInt (Main.MUSIC) == 0) {
			music.spriteName = "sound_close";
		} else {
			music.spriteName = "sound_open";
		}
	}
}
