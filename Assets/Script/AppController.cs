﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppController : MonoBehaviour {
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
	public static void Init()
	{
		if (GameObject.Find("AppController")) return;
		GameObject o = new GameObject("AppController");
		o.AddComponent(typeof(AppController));
		DontDestroyOnLoad(o);
	}
	void OnApplicationFocus(bool hasFocus)
	{
		if(hasFocus)
		{
			Main.isConnected = false;
		}
	}
}
