﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour {

	private static EffectManager instance = null;
	public AudioSource audioMgr;

	private AudioClip ac;
	private string curMusicName = "";

	public static EffectManager Instance
	{
		get
		{ 
			return instance;
		}
	}

	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad(this.gameObject);
	}

	public void  Play(string fileName)
	{
		if (PlayerPrefs.GetInt (Main.EFFECT) == 0) {
			return;
		}
		ac = Resources.Load("Music/"+fileName) as AudioClip;
		if (ac == null) {
			Debug.Log ("没有找到资源文件:Music/"+fileName);
		}
		audioMgr.clip = ac;
		audioMgr.Play();
		curMusicName = fileName;
	}

	public void Play(){
		audioMgr.Play();
	}

	public void Pause(){
		audioMgr.Pause ();
	}

	public void Stop()
	{
		audioMgr.Stop();
		curMusicName = "";
		Debug.Log("Stop background music");
	}

	public void PlayClip(AudioClip clip){
		audioMgr.clip = clip;
		audioMgr.Play();
	}
}
