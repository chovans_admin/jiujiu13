﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System; 

public class LoginController : BaseController {

	public GameObject rootPanel;
	public GameObject weixinLoginButton;
	public GameObject border;
	public GameObject check;

	public GameObject rulePanel;

	public UIPanel waitPanel;

	// Use this for initialization
	void Start () {
		Debug.Log ("登录界面");
		check.SetActive (true);
		rulePanel.SetActive (false);
		weixinLoginButton.GetComponent<UIButton> ().isEnabled = true;

		//初始化socket
		Main.Instance.init ();
		//初始化视图
		AsyncImageDownload.CreateSingleton ();

		if (PlayerPrefs.GetInt (Main.MUSIC) == 0) {
			AudioManager.Instance.Pause ();
		} else {
			AudioManager.Instance.Play ();
		}
			
		Application.targetFrameRate = 60;  

		//在接收者中注册事件及其回调方法
		NotificationCenter.Get().AddEventListener(API.ROOM_ACTION, roomResult);
	}

	void Update(){
		base.Update ();
//		#if UNITY_ANDROID || UNITY_IOS
//		if (Main.user.id != "" && Main.user.status == USER_STATUS.FREE.GetHashCode ()) {
//			SceneManager.LoadScene ("Home");	
//		}
//		#endif
	}

	void OnDestroy(){
		Debug.Log ("移除监听");
		NotificationCenter.Get ().RemoveEventListener (API.ROOM_ACTION,roomResult);
	}
		
//	public void login(string code){
//		if (code == null || code == "") {
//			waitPanel.gameObject.SetActive (false);
//			return;
//		} else {
//			waitPanel.gameObject.SetActive (true);
//		}
//
//		API.Login (code,isNative);	
//
//	}
		
	public void cancelLogin(){
		waitPanel.gameObject.SetActive (false);
	}

//	public void weixinLoginResult(object result){
//
//
//		#if UNITY_EDITOR
//		if (Main.user.status == USER_STATUS.FREE.GetHashCode ()) {
//			SceneManager.LoadScene ("Home");	
//		} else {
//			Debug.Log ("检测状态为非Free");
//		}	
//		#endif
//	}

	public void roomResult(object result){
		Utils.checkRoomInfo (result);
	}


	public void accpet(){
		if (check.activeInHierarchy) {
			TweenScale.Begin (check.gameObject, 0.1f, new Vector3(0,0,0));	
		} else {
			TweenScale.Begin (check.gameObject, 0.1f, new Vector3(1,1,1));	
		}

		StartCoroutine (DelayToInvoke.DelayToInvokeDo (() => {
			check.SetActive (!check.activeInHierarchy);
			weixinLoginButton.GetComponent<UIButton> ().isEnabled = check.activeInHierarchy;
		}, 0.1f));


	}

	public void CloseRule(){
		rulePanel.SetActive (false);
	}

	public void OpenRule(){
		rulePanel.SetActive (true);
	}
}
