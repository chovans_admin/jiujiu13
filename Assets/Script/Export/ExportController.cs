﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExportController : BaseController {


	public List<UISprite> menus;
	public List<UISprite> contents;

	// Use this for initialization
	void Start () {
		//添加事件
		foreach(var menu in menus){
			UIEventListener.Get (menu.gameObject).onClick += SelectMenu;
		}

		SelectMenu (menus [0].gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void SelectMenu(GameObject gameObject){
		foreach (var menu in menus) {
			menu.spriteName = menu.spriteName.Substring(0,menu.spriteName.Length-1) + "2";
			if (menu.gameObject == gameObject) {
				menu.spriteName = menu.spriteName.Substring(0,menu.spriteName.Length-1) + "1";
				SelectContent (menu);
			}
		}
	}

	private void SelectContent(UISprite sprite){
		foreach (var content in contents) {
			content.gameObject.SetActive (false);
		}
		contents [menus.IndexOf (sprite)].gameObject.SetActive (true);
	}

	public void exit(){
		EffectUtil.DianJi();
		SceneManager.LoadScene ("Home");
	}
}
